﻿using FoodClasses.Models.Config.Deal;
using FoodClasses.Models.Config.Security;
using FoodClasses.Models.Entities.Deal;
using FoodClasses.Models.Entities.Security;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodClasses.Models
{
    public class FoodContext : DbContext
    {
        // DBSETS
        public DbSet<Rol> Rol { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<Authentication> Authentication { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<DocumentType> DocumentType { get; set; }
        public DbSet<Gallery> Gallery { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<WayToPay> WayToPay { get; set; }
        public DbSet<Client> Client { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<OrderDetail> OrderDetail { get; set; }
        public DbSet<Company> Company { get; set; }
        public DbSet<OrderState> OrderState { get; set; }
        public DbSet<Service> Service { get; set; }

        public FoodContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Security
            AuthenticationConfig.SetEntityBuilder(modelBuilder.Entity<Authentication>());
            UserConfig.SetEntityBuilder(modelBuilder.Entity<User>());
            RolConfig.SetEntityBuilder(modelBuilder.Entity<Rol>());

            // Deal
            CategoryConfig.SetEntityBuilder(modelBuilder.Entity<Category>());
            ClientConfig.SetEntityBuilder(modelBuilder.Entity<Client>());
            DocumentTypeConfig.SetEntityBuilder(modelBuilder.Entity<DocumentType>());
            GalleryConfig.SetEntityBuilder(modelBuilder.Entity<Gallery>());
            OrderConfig.SetEntityBuilder(modelBuilder.Entity<Order>());
            OrderDetailConfig.SetEntityBuilder(modelBuilder.Entity<OrderDetail>());
            ProductConfig.SetEntityBuilder(modelBuilder.Entity<Product>());
            WayToPayConfig.SetEntityBuilder(modelBuilder.Entity<WayToPay>());
            ServiceConfig.SetEntityBuilder(modelBuilder.Entity<Service>());

            base.OnModelCreating(modelBuilder);
        }
    }
}
