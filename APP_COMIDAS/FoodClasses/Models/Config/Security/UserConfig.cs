﻿using FoodClasses.Models.Entities.Security;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodClasses.Models.Config.Security
{
    public class UserConfig
    {
        public static void SetEntityBuilder(EntityTypeBuilder<User> entityBuilder)
        {
            entityBuilder.ToTable("user");

            entityBuilder.HasKey(x => x.id);
            entityBuilder.Property(x => x.name).IsRequired();
            entityBuilder.Property(x => x.lastname).IsRequired();
            entityBuilder.Property(x => x.email).IsRequired();
            entityBuilder.Property(x => x.password).IsRequired();
            entityBuilder.Property(x => x.rol_id).IsRequired();
            entityBuilder.Property(x => x.notify).IsRequired();
            entityBuilder.Property(x => x.phone).HasDefaultValue(null);
            entityBuilder.Property(x => x.state).IsRequired();

            entityBuilder.HasOne(x => x.Rol).WithMany(x => x.Users).HasForeignKey(x => x.rol_id);
        }
    }
}
