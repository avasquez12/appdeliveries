﻿using FoodClasses.Models.Entities.Security;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodClasses.Models.Config.Security
{
    public class RolConfig
    {
        public static void SetEntityBuilder(EntityTypeBuilder<Rol> entityBuilder)
        {
            entityBuilder.ToTable("rol");

            entityBuilder.HasKey(x => x.id);
            entityBuilder.Property(x => x.description).IsRequired();
            entityBuilder.Property(x => x.state).IsRequired();
        }
    }
}
