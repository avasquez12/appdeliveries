﻿using FoodClasses.Models.Entities.Security;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodClasses.Models.Config.Security
{
    public class AuthenticationConfig
    {
        public static void SetEntityBuilder(EntityTypeBuilder<Authentication> entityBuilder)
        {
            entityBuilder.ToTable("authentication");

            entityBuilder.HasKey(x => x.id);
            entityBuilder.Property(x => x.refresh_token).IsRequired();
            entityBuilder.Property(x => x.token).IsRequired();
            entityBuilder.Property(x => x.user_id).HasDefaultValue(null);
            entityBuilder.Property(x => x.client_id).HasDefaultValue(null);

            entityBuilder.HasOne(x => x.User).WithOne(x => x.Authentication).HasForeignKey<Authentication>(x => x.user_id);
            entityBuilder.HasOne(x => x.Client).WithOne(x => x.Authentication).HasForeignKey<Authentication>(x => x.client_id);
        }
    }
}
