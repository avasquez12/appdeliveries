﻿using FoodClasses.Models.Entities.Deal;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodClasses.Models.Config.Deal
{
    public class OrderDetailConfig
    {
        public static void SetEntityBuilder(EntityTypeBuilder<OrderDetail> entityBuilder)
        {
            entityBuilder.ToTable("order_detail");

            entityBuilder.HasKey(x => x.id);
            entityBuilder.Property(x => x.product_id).HasDefaultValue(null);
            entityBuilder.Property(x => x.service_id).HasDefaultValue(null);
            entityBuilder.Property(x => x.quantity).HasDefaultValue(null);
            entityBuilder.Property(x => x.price).IsRequired();

            entityBuilder.HasOne(x => x.Product).WithMany(x => x.OrdersDetail).HasForeignKey(x => x.product_id);
            entityBuilder.HasOne(x => x.Service).WithMany(x => x.OrdersDetail).HasForeignKey(x => x.service_id);
            entityBuilder.HasOne(x => x.Order).WithMany(x => x.OrdersDetail).HasForeignKey(x => x.order_id);

        }
    }
}
