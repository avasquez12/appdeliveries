﻿using FoodClasses.Models.Entities.Deal;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodClasses.Models.Config.Deal
{
    public class CompanyConfig
    {
        public static void SetEntityBuilder(EntityTypeBuilder<Company> entityBuilder)
        {
            entityBuilder.ToTable("company");

            entityBuilder.HasKey(x => x.id);
            entityBuilder.Property(x => x.document_type_id).IsRequired();
            entityBuilder.Property(x => x.document).IsRequired();
            entityBuilder.Property(x => x.bussiness_name).IsRequired();
            entityBuilder.Property(x => x.address).IsRequired();
            entityBuilder.Property(x => x.phone).HasDefaultValue(null);
            entityBuilder.Property(x => x.cell_phone).HasDefaultValue(null);
            entityBuilder.Property(x => x.whatsapp_confirmation).IsRequired();
            entityBuilder.Property(x => x.name_contact).HasDefaultValue(null);
            entityBuilder.Property(x => x.create_date).IsRequired();
            entityBuilder.Property(x => x.email).HasDefaultValue(null);
            entityBuilder.Property(x => x.web_page).HasDefaultValue(null);
            entityBuilder.Property(x => x.opening_hours).IsRequired();

            entityBuilder.HasOne(x => x.DocumentType).WithMany(x => x.Companies).HasForeignKey(x => x.document_type_id);
        }
    }
}
