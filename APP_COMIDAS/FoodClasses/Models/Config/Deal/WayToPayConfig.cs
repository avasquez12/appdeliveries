﻿using FoodClasses.Models.Entities.Deal;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodClasses.Models.Config.Deal
{
    public class WayToPayConfig
    {
        public static void SetEntityBuilder(EntityTypeBuilder<WayToPay> entityBuilder)
        {
            entityBuilder.ToTable("way_to_pay");

            entityBuilder.HasKey(x => x.id);
            entityBuilder.Property(x => x.description).IsRequired();
            entityBuilder.Property(x => x.state).IsRequired();
        }
    }
}
