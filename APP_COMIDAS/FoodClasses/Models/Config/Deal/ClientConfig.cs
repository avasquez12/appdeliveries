﻿using FoodClasses.Models.Entities.Deal;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodClasses.Models.Config.Deal
{
    public class ClientConfig
    {
        public static void SetEntityBuilder(EntityTypeBuilder<Client> entityBuilder)
        {
            entityBuilder.ToTable("client");

            entityBuilder.HasKey(x => x.id);
            entityBuilder.Property(x => x.document).HasDefaultValue(null);
            entityBuilder.Property(x => x.name).IsRequired();
            entityBuilder.Property(x => x.lastname).IsRequired();
            entityBuilder.Property(x => x.address).IsRequired();
            entityBuilder.Property(x => x.phone).IsRequired();
            entityBuilder.Property(x => x.data_treatment).IsRequired();
            entityBuilder.Property(x => x.document_type_id).HasDefaultValue(null);
            entityBuilder.Property(x => x.state).IsRequired();
            entityBuilder.Property(x => x.email).IsRequired();
            entityBuilder.Property(x => x.password).IsRequired();

            entityBuilder.HasOne(x => x.DocumentType).WithMany(x => x.Clients).HasForeignKey(x => x.document_type_id);
        }
    }
}
