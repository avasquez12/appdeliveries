﻿using FoodClasses.Models.Entities.Deal;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodClasses.Models.Config.Deal
{
    public class GalleryConfig
    {
        public static void SetEntityBuilder(EntityTypeBuilder<Gallery> entityBuilder)
        {
            entityBuilder.ToTable("gallery");

            entityBuilder.HasKey(x => x.id);
            entityBuilder.Property(x => x.name).IsRequired();
            entityBuilder.Property(x => x.product_id).IsRequired();

            entityBuilder.HasOne(x => x.Product).WithMany(x => x.Galleries).HasForeignKey(x => x.product_id);
        }
    }
}
