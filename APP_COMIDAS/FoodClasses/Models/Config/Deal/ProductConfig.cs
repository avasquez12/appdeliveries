﻿using FoodClasses.Models.Entities.Deal;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodClasses.Models.Config.Deal
{
    public class ProductConfig
    {
        public static void SetEntityBuilder(EntityTypeBuilder<Product> entityBuilder)
        {
            entityBuilder.ToTable("product");

            entityBuilder.HasKey(x => x.id);
            entityBuilder.Property(x => x.name).IsRequired();
            entityBuilder.Property(x => x.category_id).IsRequired();
            entityBuilder.Property(x => x.stock).IsRequired();
            entityBuilder.Property(x => x.stock_min).IsRequired();
            entityBuilder.Property(x => x.price).IsRequired();
            entityBuilder.Property(x => x.state).IsRequired();

            entityBuilder.HasOne(x => x.Category).WithMany(x => x.Products).HasForeignKey(x => x.category_id);
        }
    }
}
