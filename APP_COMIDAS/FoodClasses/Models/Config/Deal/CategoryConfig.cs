﻿using FoodClasses.Models.Entities.Deal;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodClasses.Models.Config.Deal
{
    public class CategoryConfig
    {
        public static void SetEntityBuilder(EntityTypeBuilder<Category> entityBuilder)
        {
            entityBuilder.ToTable("category");

            entityBuilder.HasKey(x => x.id);
            entityBuilder.Property(x => x.name).IsRequired();
            entityBuilder.Property(x => x.name_file).IsRequired();
            entityBuilder.Property(x => x.state).IsRequired();
        }
    }
}
