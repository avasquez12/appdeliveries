﻿using FoodClasses.Models.Entities.Deal;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodClasses.Models.Config.Deal
{
    public class OrderStateConfig
    {
        public static void SetEntityBuilder(EntityTypeBuilder<OrderState> entityBuilder)
        {
            entityBuilder.ToTable("order_state");

            entityBuilder.HasKey(x => x.id);
            entityBuilder.Property(x => x.description).IsRequired();
            entityBuilder.Property(x => x.state).IsRequired();
        }
    }
}
