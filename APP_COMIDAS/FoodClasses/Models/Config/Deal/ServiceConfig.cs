﻿using FoodClasses.Models.Entities.Deal;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodClasses.Models.Config.Deal
{
    public class ServiceConfig
    {
        public static void SetEntityBuilder(EntityTypeBuilder<Service> entityBuilder)
        {
            entityBuilder.ToTable("service");

            entityBuilder.HasKey(x => x.id);
            entityBuilder.Property(x => x.name).IsRequired();
            entityBuilder.Property(x => x.category_id).IsRequired();
            entityBuilder.Property(x => x.schedule).HasDefaultValue(null);
            entityBuilder.Property(x => x.price).IsRequired();
            entityBuilder.Property(x => x.observation).HasDefaultValue(null);
            entityBuilder.Property(x => x.state).IsRequired();

            entityBuilder.HasOne(x => x.Category).WithMany(x => x.Services).HasForeignKey(x => x.category_id);
        }
    }
}
