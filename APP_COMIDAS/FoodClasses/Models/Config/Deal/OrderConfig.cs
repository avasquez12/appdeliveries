﻿using FoodClasses.Models.Entities.Deal;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodClasses.Models.Config.Deal
{
    public class OrderConfig
    {
        public static void SetEntityBuilder(EntityTypeBuilder<Order> entityBuilder)
        {
            entityBuilder.ToTable("order");

            entityBuilder.HasKey(x => x.id);
            entityBuilder.Property(x => x.client_id).IsRequired();
            entityBuilder.Property(x => x.date).HasDefaultValue(null);
            entityBuilder.Property(x => x.way_to_pay_id).IsRequired();
            entityBuilder.Property(x => x.order_estate_id).IsRequired();

            entityBuilder.HasOne(x => x.Client).WithMany(x => x.Orders).HasForeignKey(x => x.client_id);
            entityBuilder.HasOne(x => x.WayToPay).WithMany(x => x.Orders).HasForeignKey(x => x.way_to_pay_id);
            entityBuilder.HasOne(x => x.OrderState).WithMany(x => x.Orders).HasForeignKey(x => x.order_estate_id);
        }
    }
}
