﻿using FoodClasses.Models.Entities.Deal;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodClasses.Models.Config.Deal
{
    public class DocumentTypeConfig
    {
        public static void SetEntityBuilder(EntityTypeBuilder<DocumentType> entityBuilder)
        {
            entityBuilder.ToTable("document_type");

            entityBuilder.HasKey(x => x.id);
            entityBuilder.Property(x => x.description).IsRequired();
        }
    }
}
