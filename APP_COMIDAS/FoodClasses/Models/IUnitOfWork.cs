﻿using FoodClasses.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodClasses.Models
{
    public interface IUnitOfWork
    {
        Task SaveChanges();
    }

    public class UnitOfWork : IUnitOfWork
    {
        private readonly FoodContext _context;

        public UnitOfWork(FoodContext context)
        {
            _context = context;
        }

        public Task SaveChanges()
        {
            return _context.SaveChangesAsync();
        }
    }
}
