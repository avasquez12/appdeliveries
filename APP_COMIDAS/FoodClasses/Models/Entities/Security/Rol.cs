﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodClasses.Models.Entities.Security
{
    public class Rol
    {
        public int id { get; set; }
        public string description { get; set; }
        public bool state { get; set; }


        // Virtual objects
        public virtual List<User> Users { get; set; }
    }
}
