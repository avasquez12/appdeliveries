﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Text.Json.Serialization;

namespace FoodClasses.Models.Entities.Security
{
    public class User
    {
        public int id { get; set; }
        public string name { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public bool state { get; set; }
        public int rol_id { get; set; }
        public bool notify { get; set; }
        public string phone { get; set; }

        [NotMapped]
        public string completeName
        {
            get { return $"{name} {lastname}"; }
        }



        // Virtual objects
        public virtual Rol Rol { get; set; }

        [JsonIgnore]
        public virtual Authentication Authentication { get; set; }
    }
}
