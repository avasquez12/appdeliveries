﻿using FoodClasses.Models.Entities.Deal;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodClasses.Models.Entities.Security
{
    public class Authentication
    {
        public int id { get; set; }
        public string refresh_token { get; set; }
        public string token { get; set; }
        public Nullable<int> user_id { get; set; }
        public Nullable<int> client_id { get; set; }

        // Virtual objects
        public virtual User User { get; set; }
        public virtual Client Client { get; set; }
    }
}
