﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace FoodClasses.Models.Entities.Deal
{
    public class DocumentType
    {
        public int id { get; set; }
        public string description { get; set; }


        // Virtual objects
        public virtual List<Client> Clients { get; set; }
        public virtual List<Company> Companies { get; set; }
    }
}
