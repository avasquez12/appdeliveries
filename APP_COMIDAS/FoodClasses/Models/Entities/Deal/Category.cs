﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodClasses.Models.Entities.Deal
{
    public class Category
    {
        public int id { get; set; }
        public string name { get; set; }
        public string name_file { get; set; }
        public bool state { get; set; }


        // Virtual objects
        public virtual List<Product> Products { get; set; }
        public virtual List<Service> Services { get; set; }
    }
}
