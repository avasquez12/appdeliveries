﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodClasses.Models.Entities.Deal
{
    public class OrderDetail
    {
        public int id { get; set; }
        public int order_id { get; set; }
        public Nullable<int> product_id { get; set; }
        public Nullable<int> quantity { get; set; }
        public Nullable<int> service_id { get; set; }
        public double price { get; set; }


        // Virtual objects
        public virtual Order Order { get; set; }
        public virtual Product Product { get; set; }
        public virtual Service Service { get; set; }
    }
}
