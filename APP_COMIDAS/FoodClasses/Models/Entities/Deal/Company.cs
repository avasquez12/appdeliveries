﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodClasses.Models.Entities.Deal
{
    public class Company
    {
        public int id { get; set; }
        public int document_type_id { get; set; }
        public string document { get; set; }
        public string bussiness_name { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string cell_phone { get; set; }
        public bool whatsapp_confirmation { get; set; }
        public string name_contact { get; set; }
        public DateTime create_date { get; set; }
        public string email { get; set; }
        public string web_page { get; set; }
        public string opening_hours { get; set; }


        // Virtual objects
        public virtual DocumentType DocumentType { get; set; }
    }
}
