﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodClasses.Models.Entities.Deal
{
    public class Product
    {
        public int id { get; set; }
        public string name { get; set; }
        public int category_id { get; set; }
        public int stock { get; set; }
        public int stock_min { get; set; }
        public double price { get; set; }
        public bool state { get; set; }


        // Virtual objects
        public virtual Category Category { get; set; }

        public virtual List<Gallery> Galleries { get; set; }
        public virtual List<OrderDetail> OrdersDetail { get; set; }
    }
}
