﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodClasses.Models.Entities.Deal
{
    public class Gallery
    {
        public int id { get; set; }
        public string name { get; set; }
        public int product_id { get; set; }


        // Virtual objects
        public virtual Product Product { get; set; }
    }
}
