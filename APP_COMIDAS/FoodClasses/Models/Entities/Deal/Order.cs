﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodClasses.Models.Entities.Deal
{
    public class Order
    {
        public int id { get; set; }
        public int client_id { get; set; }
        public Nullable<DateTime> validation_payment_date { get; set; }
        public int way_to_pay_id { get; set; }
        public int order_estate_id { get; set; }


        // Virtual objects
        public virtual Client Client { get; set; }
        public virtual WayToPay WayToPay { get; set; }
        public virtual OrderState OrderState { get; set; }

        public virtual List<OrderDetail> OrdersDetail { get; set; }
    }
}
