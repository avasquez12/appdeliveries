﻿using FoodClasses.Models.Entities.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Text.Json.Serialization;

namespace FoodClasses.Models.Entities.Deal
{
    public class Client
    {
        public int id { get; set; }
        public string document { get; set; }
        public string name { get; set; }
        public string lastname { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public Nullable<int> document_type_id { get; set; }
        public bool state { get; set; }
        public bool data_treatment { get; set; }
        public string email { get; set; }
        public string password { get; set; }

        [NotMapped]
        public string completeName
        {
            get { return $"{name} {lastname}"; }
        }


        // Virtual objects
        public virtual DocumentType DocumentType { get; set; }

        public virtual List<Order> Orders { get; set; }

        [JsonIgnore]
        public virtual Authentication Authentication { get; set; }
    }
}
