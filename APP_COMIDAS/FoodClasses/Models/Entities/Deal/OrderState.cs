﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodClasses.Models.Entities.Deal
{
    public class OrderState
    {
        public int id { get; set; }
        public string description { get; set; }
        public bool state { get; set; }


        // Virtual Objects
        public virtual List<Order> Orders { get; set; }
    }
}
