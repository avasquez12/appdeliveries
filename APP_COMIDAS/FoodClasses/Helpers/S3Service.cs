﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using FoodClasses.Helpers.Assistants;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace FoodClasses.Helpers
{
    public static class S3Service
    {
        private static readonly RegionEndpoint bucketRegion = RegionEndpoint.USWest2;
        private static IAmazonS3 s3Client = new AmazonS3Client("AKIATRGS6N2YO6KGVPZ5", "bFsfwV5HCrEKTG0U0r6Nni8uVG2T0UnWqupr/G0E", bucketRegion);

        private static string GetBucketName()
        {
            string bucket = "app-comidas";
            return bucket;
        }

        /// <summary>
        /// Method to upload a file to Amazon S3 from a request HTTP
        /// </summary>
        /// <param name="file">File</param>
        /// <returns></returns>
        public static async Task<ResponseModel> UploadFileAsync(IFormFile file, string company, string path)
        {
            var rm = new ResponseModel();

            try
            {
                var fileTransferUtility = new TransferUtility(s3Client);
                using(var newMemoryStream = new MemoryStream())
                {
                    file.CopyTo(newMemoryStream);
                    await fileTransferUtility.UploadAsync(newMemoryStream, $"{GetBucketName()}/{company}{path}", file.FileName);
                    rm.response = true;
                    rm.message = "";
                    rm.result = "Archivo agregado con éxito";
                }
            }
            catch (AmazonS3Exception error)
            {
                rm.message = $"Error encountered on server. Message: '{error.Message}' while writing an object";
            }
            catch(Exception error)
            {
                rm.message = $"Unknown error encountered on server. Message: '{error.Message}' while writing an object";
            }

            return rm;
        }

        /// <summary>
        /// Method to download a file from Amazon S3 from a request HTTP
        /// </summary>
        /// <param name="fileName">String</param>
        /// <returns></returns>
        public static async Task<GetObjectResponse> DownloadFileAsync(string fileName, string path, string company)
        {
            try
            {
                GetObjectRequest request = new GetObjectRequest
                {
                    BucketName = $"{GetBucketName()}/{company}{path}",
                    Key = fileName
                };
                GetObjectResponse response = await s3Client.GetObjectAsync(request);
                return response;
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                return null;
            }
        }

        // Get Object from S3
        public static string GeneratePreSignedURL(string fileName, string path, string company)
        {
            string urlString = "";
            try
            {
                GetPreSignedUrlRequest request1 = new GetPreSignedUrlRequest
                {
                    BucketName = $"{GetBucketName()}/{company}{path}",
                    Key = fileName,
                    Expires = DateTime.Now.AddMinutes(2)
                };
                urlString = s3Client.GetPreSignedURL(request1);
            }
            catch (AmazonS3Exception e)
            {
                Console.WriteLine("Error encountered on server. Message:'{0}' when writing an object", e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
            }
            return urlString;
        }

        /// <summary>
        /// Method to delete a file from Amazon S3 from a request HTTP
        /// </summary>
        /// <param name="fileName">String</param>
        /// <returns></returns>
        public static async Task<ResponseModel> DeleteFileAsync(string fileName)
        {
            var rm = new ResponseModel();

            try
            {
                var deleteObjectRequest = new DeleteObjectRequest
                {
                    BucketName = GetBucketName(),
                    Key = fileName
                };
                await s3Client.DeleteObjectAsync(deleteObjectRequest);
                rm.response = true;
            }
            catch  (AmazonS3Exception error)
            {
                rm.message = $"Error encountered on server. Message: '{error.Message}' while deleting an object";
            }
            catch (Exception error)
            {
                rm.message = $"Unknown error encountered on server. Message: '{error.Message}' while deleting an object";
            }
            return rm;
        }
    }
}
