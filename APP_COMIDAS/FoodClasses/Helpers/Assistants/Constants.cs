﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FoodClasses.Helpers.Assistants
{
    public class Constants
    {
        // Roles
        public const int ROL_SUPER = 1;

        // Order states
        public const int STATE_SENT = 1;
        public const int STATE_DELIVERED = 2;

        // To send Mails
        public const string SMTP_HOST = "smtp.zoho.com";
        public const string SMTP_USER = "soporte@fivetech.co";
        public const string SMTP_PASSWORD = "3eIPQQHc";
        public const string SMTP_FROM = "soporte@fivetech.co";
        public const int SMTP_PORT = 587;
        public const bool SMTP_SSL = true;

        // Company data
        public const string COMPANY_NAME_APP = "APP Comidas";
        public const string COMPANY_NAME = "Empresa de Comidas";
        public const string COMPANY_URI_APP = "https://app-comidas.com.co";

        // Mail messages
        public const string MAIL_MESSAGE_CREATE_CLIENT = "Gracias por registrarse";
        public const string MAIL_MESSAGE_UPDATE_CLIENT = "Sus datos fueron actualizados correctamente";
        public const string MAIL_MESSAGE_CHANGE_PASSWORD_CLIENT = "Su contraseña fué actualizada correctamente";
        public const string MAIL_MESSAGE_RECOVERY_PASSWORD_CLIENT = "Su contraseña fué restablecida correctamente";

        // Files management
        public const string FILE_CATEGORIES = "/Categories";
        public const string FILE_PRODUCTS = "/Products";
    }
}
