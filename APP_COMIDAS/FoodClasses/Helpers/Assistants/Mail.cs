﻿using FoodClasses.Helpers.Assistants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace FoodClasses.Helpers.Assistants
{
    // MailVM
    public class MailVM
    {
        public List<string> destinations { get; set; }
        public string subject { get; set; }
        public string message { get; set; }

        public MailVM()
        {
            destinations = new List<string>();
        }
    }
    public class Mail
    {
        private static MailMessage mail = new MailMessage();
        private static NetworkCredential credentials = new NetworkCredential();
        private static SmtpClient smtp = new SmtpClient();

        /// <summary>
        /// Method to send a mail
        /// </summary>
        /// <param name="model">Correo's model</param>
        public static ResponseModel Send(MailVM model)
        {
            var rm = new ResponseModel();

            // Clean MailMessage object
            mail.Attachments.Clear();
            mail.To.Clear();
            mail.Subject = "";
            mail.Body = "";

            // Read destinations
            if(model.destinations.Count > 0)
            {
                foreach (var destiny in model.destinations)
                {
                    mail.To.Add(destiny);
                }

                // Build Mail
                mail.Subject = model.subject;
                mail.BodyEncoding = Encoding.UTF8;
                mail.Body = model.message;
                mail.IsBodyHtml = true;
                mail.From = new MailAddress(Constants.SMTP_FROM);

                credentials.UserName = Constants.SMTP_USER;
                credentials.Password = Constants.SMTP_PASSWORD;

                smtp.Host = Constants.SMTP_HOST;
                smtp.Port = Constants.SMTP_PORT;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.EnableSsl = Constants.SMTP_SSL;
                smtp.Credentials = credentials;

                try
                {
                    smtp.Send(mail);
                    rm.response = true;
                    rm.result = "Email enviado correctamente";
                }
                catch (Exception error)
                {
                    rm.message = error.ToString();
                }
            }

            return rm;
        }
    }
}
