﻿using APP_COMIDAS.ViewModels.Deal;
using FoodClasses.Models.Entities.Deal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP_COMIDAS.Mappers
{
    public class CompanyMapper
    {
        public static GetCompanyVM MapCompanyToGetCompanyVM(Company company)
        {
            return new GetCompanyVM()
            {
                company_name = company.bussiness_name,
                address = company.address,
                phone = company.phone,
                cell_phone = company.cell_phone,
                confirmation_whatsapp = company.whatsapp_confirmation,
                email = company.email,
                web_page = company.web_page,
                opening_hours = company.opening_hours
            };
        }
    }
}
