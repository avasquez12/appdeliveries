﻿using APP_COMIDAS.Helpers.Assistants;
using APP_COMIDAS.ViewModels.Deal.Clients;
using FoodClasses.Models.Entities.Deal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP_COMIDAS.Mappers
{
    public class ClientMapper
    {
        public static Client MapAddClientFromSelfVMToClient(AddClientFromSelfVM clientVM)
        {
            return new Client()
            {
                document_type_id = clientVM.document_type_id != null ? clientVM.document_type_id : null,
                document = clientVM.document != null ? clientVM.document : null,
                name = clientVM.name,
                lastname = clientVM.lastname,
                address = clientVM.address,
                phone = clientVM.phone,
                state = true,
                data_treatment = true,
                email = clientVM.email,
                password = Encryption.GetMD5(clientVM.password)
            };
        }
    }
}
