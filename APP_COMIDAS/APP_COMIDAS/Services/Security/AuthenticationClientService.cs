﻿using APP_COMIDAS.Config;
using APP_COMIDAS.Helpers.Assistants;
using APP_COMIDAS.Repositories.Security;
using APP_COMIDAS.ViewModels.Security;
using FoodClasses.Helpers.Assistants;
using FoodClasses.Models;
using FoodClasses.Models.Entities.Security;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace APP_COMIDAS.Services.Security
{
    // Interface
    public interface IAuthenticationClientService
    {
        Task<ResponseModel> Login(LoginVM credentials);
        Task<ResponseModel> Refresh(string refresh);
    }

    public class AuthenticationClientService : IAuthenticationClientService
    {
        private readonly FoodContext _context;
        private readonly IOptions<AppSettings> _appSettings;

        public AuthenticationClientService(FoodContext context,
            IOptions<AppSettings> appSettings)
        {
            _context = context;
            _appSettings = appSettings;
        }

        private string GenerateJWT(int id)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Value.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public async Task<ResponseModel> Login(LoginVM credentials)
        {
            var rm = new ResponseModel();
            string passwordMD5 = Encryption.GetMD5(credentials.password);

            try
            {
                var client = await _context.Client.Where(x => x.email == credentials.email).SingleOrDefaultAsync();
                if(client != null)
                {
                    if(client.password == passwordMD5)
                    {
                        rm.result = new AuthenticationClientVM
                        {
                            Client = client,
                            token = GenerateJWT(client.id),
                            refreshToken = Functions.GenerateRefreshToken()
                        };

                        var auth = _context.Authentication.Where(x => x.client_id == client.id).SingleOrDefault();
                        if(auth == null)
                        {
                            await _context.Authentication.AddAsync(new Authentication
                            {
                                Client = client,
                                token = GenerateJWT(client.id),
                                refresh_token = Functions.GenerateRefreshToken(),
                                client_id = client.id
                            });
                        }
                        else
                        {
                            auth.refresh_token = rm.result.refreshToken;
                            auth.token = rm.result.token;
                            _context.Authentication.Update(auth);
                        }
                        await _context.SaveChangesAsync();
                        rm.message = "La acción se completó con éxito;";
                        rm.response = true;

                        return rm;
                    }
                    else
                    {
                        rm.message = "Las credenciales ingresadas son incorrectas";
                    }
                }
                else
                {
                    rm.message = "El Cliente ingresado no existe";
                }
            }
            catch (Exception error)
            {
                rm.response = false;
                rm.message = error.ToString();
            }

            return rm;
        }

        public async Task<ResponseModel> Refresh(string refresh)
        {
            var rm = new ResponseModel();

            try
            {
                var auth = _context.Authentication.Where(x => x.refresh_token == refresh).SingleOrDefault();
                if(auth != null)
                {
                    auth.token = GenerateJWT((int)auth.client_id);
                    auth.refresh_token = Functions.GenerateRefreshToken();

                    _context.Authentication.Update(auth);
                    await _context.SaveChangesAsync();

                    rm.response = true;
                    rm.result = new RefreshTokenViewModel
                    {
                        token = auth.token,
                        refresh = auth.refresh_token
                    };
                }
                else
                {
                    rm.response = false;
                    rm.message = "No existe un token para refrescar";
                }
            }
            catch (Exception error)
            {
                rm.message = error.ToString();
            }

            return rm;
        }
    }
}
