﻿using APP_COMIDAS.Repositories.Security;
using FoodClasses.Helpers.Assistants;
using FoodClasses.Models.Entities.Security;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace APP_COMIDAS.Services.Security
{
    // Interface
    public interface IRolService
    {
        Task<ResponseModel> Get(int id);
        Task<ResponseModel> GetAll();
    }

    // Class
    public class RolService : IRolService
    {
        private readonly IRolRepository _rolRepository;
        private readonly ILogger<RolService> _logger;

        public RolService(IRolRepository rolRepository, ILogger<RolService> logger)
        {
            _rolRepository = rolRepository;
            _logger = logger;
        }


        public async Task<ResponseModel> Get(int id)
        {
            var rm = new ResponseModel();
            try
            {
                var rol = await _rolRepository.Get(id);

                if (rol != null)
                {
                    rm.response = true;
                    rm.message = "";
                    rm.result = rol;
                }
            }
            catch (Exception e)
            {
                _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                _logger.LogError(e, e.Message);
            }
            return rm;
        }

        public async Task<ResponseModel> GetAll()
        {
            var rm = new ResponseModel();
            try
            {
                rm.result = await _rolRepository.GetAll();
                rm.response = true;
                rm.message = "";
            }
            catch (Exception e)
            {
                _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                _logger.LogError(e, e.Message);
            }
            return rm;
        }
    }
}
