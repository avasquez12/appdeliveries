﻿using APP_COMIDAS.Helpers.Assistants;
using APP_COMIDAS.Repositories.Deal;
using APP_COMIDAS.ViewModels.Deal.Categories;
using FoodClasses.Helpers;
using FoodClasses.Helpers.Assistants;
using FoodClasses.Models;
using FoodClasses.Models.Entities.Deal;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace APP_COMIDAS.Services.Deal
{
    public class AddCategoryVM
    {
        public string name { get; set; }
        public IFormFile file_category { get; set; }
    }
    // Interface
    public interface ICategoryService
    {
        Task<ResponseModel> AddCategory(AddCategoryVM model);
        Task<ResponseModel> GetCategory(int category_id);
        Task<ResponseModel> GetAllCategories();
    }

    // Class
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly ILogger<CategoryService> _logger;
        private readonly IUnitOfWork _unit;

        public CategoryService(ICategoryRepository categoryRepository,
            ILogger<CategoryService> logger,
            IUnitOfWork unit)
        {
            _categoryRepository = categoryRepository;
            _logger = logger;
            _unit = unit;
        }

        public async Task<ResponseModel> AddCategory(AddCategoryVM model)
        {
            var rm = new ResponseModel();
            try
            {
                Category category = new Category();
                category.name = model.name;
                category.name_file = model.file_category.FileName;
                category.state = true;
                var addCategory = await _categoryRepository.Add(category);
                await _unit.SaveChanges();
                rm.SetResponse(true, "La categoría fue salvada con éxito");

                if(addCategory != null)
                {
                    try
                    {
                        rm.result = await S3Service.UploadFileAsync(model.file_category, Constants.COMPANY_NAME, Constants.FILE_CATEGORIES);
                        rm.message = "Uploaded file";
                    }
                    catch (Exception error)
                    {
                        rm.message = error.Message;
                    }
                }

            }
            catch (Exception e)
            {
                _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                _logger.LogError(e, e.Message);
            }
            return rm;
        }

        public async Task<ResponseModel> GetCategory(int category_id)
        {
            var rm = new ResponseModel();
            try
            {
                var category = await _categoryRepository.Get(category_id);

                if(category != null)
                {
                    rm.result = new GetCategoryVM()
                    {
                        name = category.name,
                        url_file = S3Service.GeneratePreSignedURL(category.name_file, Constants.FILE_CATEGORIES, Constants.COMPANY_NAME)
                    };
                    rm.response = true;
                }
            }
            catch (Exception e)
            {
                _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                _logger.LogError(e, e.Message);
            }
            return rm;
        }

        public async Task<ResponseModel> GetAllCategories()
        {
            var rm = new ResponseModel();
            try
            {
                var categories = await _categoryRepository.GetAll();

                if (categories.Count() > 0)
                {
                    var listCategories = new List<GetCategoryVM>();
                    foreach (var category in categories)
                    {
                        GetCategoryVM obj = new GetCategoryVM();
                        obj.name = category.name;
                        obj.url_file = S3Service.GeneratePreSignedURL(category.name_file, Constants.FILE_CATEGORIES, Constants.COMPANY_NAME);
                        listCategories.Add(obj);
                    }
                    rm.result = listCategories;
                    rm.response = true;
                    rm.message = "";
                }
            }
            catch (Exception e)
            {
                _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                _logger.LogError(e, e.Message);
            }
            return rm;
        }
    }
}
