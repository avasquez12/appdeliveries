﻿using APP_COMIDAS.Helpers.Assistants;
using APP_COMIDAS.Repositories.Deal;
using APP_COMIDAS.ViewModels.Deal;
using FoodClasses.Helpers.Assistants;
using FoodClasses.Models;
using FoodClasses.Models.Entities.Deal;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using static APP_COMIDAS.Helpers.Assistants.ExtensionMethods;
using static APP_COMIDAS.ViewModels.Deal.AddOrderVM;
using static APP_COMIDAS.ViewModels.Deal.GetOrderByClientVM;

namespace APP_COMIDAS.Services.Deal
{
    // Interface
    public interface IOrderService
    {
        Task<ResponseModel> RegisterOrder(AddOrderVM model);
        Task<ResponseModel> GetOrderByClient(int client_id);
    }

    // Class
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderDetailRepository _orderDetailRepository;
        private readonly IClientRepository _clientRepository;
        private readonly IWayToPayRepository _wayToPayRepository;
        private readonly IOrderStateRepository _orderStateRepository;
        private readonly IProductRepository _productRepository;
        private readonly ILogger<OrderService> _logger;
        private readonly IUnitOfWork _unit;

        public OrderService(IOrderRepository orderRepository,
            IOrderDetailRepository orderDetailRepository,
            IClientRepository clientRepository,
            IWayToPayRepository wayToPayRepository,
            IOrderStateRepository orderStateRepository,
            IProductRepository productRepository,
            ILogger<OrderService> logger,
            IUnitOfWork unit)
        {
            _orderRepository = orderRepository;
            _orderDetailRepository = orderDetailRepository;
            _clientRepository = clientRepository;
            _wayToPayRepository = wayToPayRepository;
            _orderStateRepository = orderStateRepository;
            _productRepository = productRepository;
            _logger = logger;
            _unit = unit;
        }

        public async Task<ResponseModel> RegisterOrder(AddOrderVM model)
        {
            var rm = new ResponseModel();
            try
            {
                // Build Order Object
                Order order = new Order();
                // Query to virtual objects from Order
                Client client = await _clientRepository.Get(model.client_id);
                WayToPay wtp = await _wayToPayRepository.Get(model.way_to_pay_id);
                OrderState state = await _orderStateRepository.Get(Constants.STATE_SENT);

                order.client_id = model.client_id;
                order.Client = client;
                order.way_to_pay_id = model.way_to_pay_id;
                order.WayToPay = wtp;
                order.order_estate_id = Constants.STATE_SENT;
                order.OrderState = state;
                await _orderRepository.Add(order);

                // Read the OrderDetail List
                foreach (var detail in model.OrderDetail)
                {
                    // Build OrderDetail Object
                    OrderDetail orderDetail = new OrderDetail();
                    // Query to virtual objects from OrderDetail
                    Product product = await _productRepository.Get(detail.product_id);

                    orderDetail.Product = product;
                    orderDetail.product_id = detail.product_id;
                    orderDetail.Order = order;
                    orderDetail.quantity = detail.quantity;
                    orderDetail.price = product.price;
                    await _orderDetailRepository.Add(orderDetail);
                }

                await _unit.SaveChanges();
                rm.response = true;
                rm.message = "";
                rm.result = "El pedido fué guardado correctamente";
            }
            catch (Exception e)
            {
                _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                _logger.LogError(e, e.Message);
            }
            return rm;
        }

        public async Task<ResponseModel> GetOrderByClient(int client_id)
        {
            var rm = new ResponseModel();
            try
            {
                // Get orders by client
                var orders = await _orderRepository.GetAllOrderByClient(client_id);

                rm.result = orders.Select(x => new GetOrderByClientVM()
                {
                    way_to_pay = x.WayToPay.description,
                    estado_pedido = x.OrderState.description,
                    total = ExtensionMethods.GetTotalPrice(x.OrdersDetail
                    .GroupBy(x => x.order_id).Select(od => new GroupingOrderDetailByOrderVM
                    {
                        order_id = od.Key,
                        orderDetails = od.Select(o => o).ToList()
                    })),
                    details = x.OrdersDetail.Select(o => new GetOrderDetail()
                    {
                        product = o.Product.name,
                        quantity = (int)o.quantity,
                        price = o.price
                    })
                });
                rm.response = true;
                rm.message = "Operación realizada con éxito";
            }
            catch (Exception e)
            {
                _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                _logger.LogError(e, e.Message);
            }
            return rm;
        }
    }
}
