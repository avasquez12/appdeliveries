﻿using APP_COMIDAS.Repositories.Deal;
using APP_COMIDAS.ViewModels.Deal.Products;
using FoodClasses.Helpers;
using FoodClasses.Helpers.Assistants;
using FoodClasses.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace APP_COMIDAS.Services.Deal
{
    // Interface
    public interface IServiceService
    {
        Task<ResponseModel> GetService(int service_id);
        Task<ResponseModel> GetAllServices();
        Task<ResponseModel> GetAllServicesByCategory(int category_id, int pagination_number);
    }

    // Class
    public class ServiceService : IServiceService
    {
        private readonly IServiceRepository _serviceRepository;
        private readonly ILogger<ServiceService> _logger;
        private readonly IUnitOfWork _unit;

        public ServiceService(IServiceRepository serviceRepository,
            ILogger<ServiceService> logger,
            IUnitOfWork unit)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
            _unit = unit;
        }


        public async Task<ResponseModel> GetService(int service_id)
        {
            var rm = new ResponseModel();
            try
            {
                var service = await _serviceRepository.Get(service_id);

                if (service != null)
                {
                    rm.result = service;
                    rm.response = true;
                }
            }
            catch (Exception e)
            {
                _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                _logger.LogError(e, e.Message);
            }
            return rm;
        }

        public async Task<ResponseModel> GetAllServices()
        {
            var rm = new ResponseModel();
            try
            {
                var services = await _serviceRepository.GetAll();

                if (services.Count() > 0)
                {
                    rm.result = services;
                    rm.response = true;
                    rm.message = "";
                }
            }
            catch (Exception e)
            {
                _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                _logger.LogError(e, e.Message);
            }
            return rm;
        }

        public async Task<ResponseModel> GetAllServicesByCategory(int category_id, int pagination_number)
        {
            var rm = new ResponseModel();
            try
            {
                var services = await _serviceRepository.GetAllServiceByCategory(category_id, pagination_number);

                if (services.Count() > 0)
                {
                    rm.result = services;
                    rm.SetResponse(true, "");
                }
            }
            catch (Exception e)
            {
                _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                _logger.LogError(e, e.Message);
            }
            return rm;
        }
    }
}
