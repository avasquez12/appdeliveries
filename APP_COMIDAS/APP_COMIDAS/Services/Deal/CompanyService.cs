﻿using APP_COMIDAS.Mappers;
using APP_COMIDAS.Repositories.Deal;
using FoodClasses.Helpers.Assistants;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace APP_COMIDAS.Services.Deal
{
    // Interface
    public interface ICompanyService
    {
        Task<ResponseModel> Get(int id);
    }

    // Class
    public class CompanyService : ICompanyService
    {
        private readonly ICompanyRepository _companyRepository;
        private readonly ILogger<CompanyService> _logger;

        public CompanyService(ICompanyRepository companyRepository, ILogger<CompanyService> logger)
        {
            _companyRepository = companyRepository;
            _logger = logger;
        }


        public async Task<ResponseModel> Get(int id)
        {
            var rm = new ResponseModel();
            try
            {
                var company = await _companyRepository.Get(id);

                if (company != null)
                {
                    rm.response = true;
                    rm.message = "";
                    rm.result = CompanyMapper.MapCompanyToGetCompanyVM(company);
                }
            }
            catch (Exception e)
            {
                _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                _logger.LogError(e, e.Message);
            }
            return rm;
        }
    }
}
