﻿using APP_COMIDAS.Repositories.Deal;
using APP_COMIDAS.ViewModels.Deal.Products;
using FoodClasses.Helpers;
using FoodClasses.Helpers.Assistants;
using FoodClasses.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace APP_COMIDAS.Services.Deal
{
    // Interface
    public interface IProductService
    {
        Task<ResponseModel> GetProduct(int product_id);
        Task<ResponseModel> GetAllProducts();
        Task<ResponseModel> GetAllProductsByCategory(int category_id, int pagination_number);
    }

    // Class
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;
        private readonly IGalleryRepository _galleryRepository;
        private readonly ILogger<ProductService> _logger;
        private readonly IUnitOfWork _unit;

        public ProductService(IProductRepository productRepository,
            IGalleryRepository galleryRepository,
            ILogger<ProductService> logger,
            IUnitOfWork unit)
        {
            _productRepository = productRepository;
            _galleryRepository = galleryRepository;
            _logger = logger;
            _unit = unit;
        }


        public async Task<ResponseModel> GetProduct(int product_id)
        {
            var rm = new ResponseModel();
            try
            {
                var product = await _productRepository.Get(product_id);

                if (product != null)
                {
                    rm.result = product;
                    rm.response = true;
                }
            }
            catch (Exception e)
            {
                _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                _logger.LogError(e, e.Message);
            }
            return rm;
        }

        public async Task<ResponseModel> GetAllProducts()
        {
            var rm = new ResponseModel();
            try
            {
                var products = await _productRepository.GetAll();

                if (products.Count() > 0)
                {
                    rm.result = products;
                    rm.response = true;
                    rm.message = "";
                }
            }
            catch (Exception e)
            {
                _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                _logger.LogError(e, e.Message);
            }
            return rm;
        }

        public async Task<ResponseModel> GetAllProductsByCategory(int category_id, int pagination_number)
        {
            var rm = new ResponseModel();
            try
            {
                var products = await _productRepository.GetAllProductByCategory(category_id, pagination_number);

                if (products.Count() > 0)
                {
                    // Gallery's product image
                    var photos = await _galleryRepository.GetAllGalleryFilterByIdProduct(products.Select(x => x.id));

                    rm.result = from p in products
                                join g in photos on p.id equals g.product_id
                                select new GetAllProductByCategoryVM()
                                {
                                    id = p.id,
                                    name = p.name,
                                    price = p.price,
                                    stock = p.stock,
                                    url_file = S3Service.GeneratePreSignedURL(g.name, Constants.FILE_PRODUCTS, Constants.COMPANY_NAME)
                                };
                    rm.SetResponse(true, "");
                }
            }
            catch (Exception e)
            {
                _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                _logger.LogError(e, e.Message);
            }
            return rm;
        }
    }
}
