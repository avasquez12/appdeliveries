﻿using APP_COMIDAS.Helpers.Assistants;
using APP_COMIDAS.Mappers;
using APP_COMIDAS.Repositories.Deal;
using APP_COMIDAS.ViewModels.Deal;
using APP_COMIDAS.ViewModels.Deal.Clients;
using FoodClasses.Helpers.Assistants;
using FoodClasses.Models;
using FoodClasses.Models.Entities.Deal;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace APP_COMIDAS.Services.Deal
{
    // Interface
    public interface IClientService
    {
        Task<ResponseModel> UpdateClientFromSelf(UpdateClientFromSelfVM model, int client_id);
        Task<ResponseModel> RegisterClientFromSelf(AddClientFromSelfVM model);
    }

    // Class
    public class ClientService : IClientService
    {
        private readonly IClientRepository _clientRepository;
        private readonly IDocumentTypeRepository _typeRepository;
        private readonly ILogger<ClientService> _logger;
        private readonly IUnitOfWork _unit;

        public ClientService(IClientRepository clientRepository,
            IDocumentTypeRepository typeRepository,
            ILogger<ClientService> logger,
            IUnitOfWork unit)
        {
            _clientRepository = clientRepository;
            _typeRepository = typeRepository;
            _logger = logger;
            _unit = unit;
        }

        public async Task<ResponseModel> RegisterClientFromSelf(AddClientFromSelfVM model)
        {
            var rm = new ResponseModel();

            try
            {
                // Map to Client object
                Client client = ClientMapper.MapAddClientFromSelfVMToClient(model);

                // Document type virtual object
                if (model.document_type_id != null)
                {
                    var documentType = await _typeRepository.Get((int)client.document_type_id);
                    client.DocumentType = documentType;
                }

                // Save changes
                await _clientRepository.Add(client);
                await _unit.SaveChanges();

                rm.SetResponse(true, $"{client.completeName} fué creado con éxito");
                rm.result = rm.message;
            }
            catch (Exception e)
            {
                _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                _logger.LogError(e, e.Message);
            }

            return rm;
        }

        public async Task<ResponseModel> UpdateClientFromSelf(UpdateClientFromSelfVM model, int client_id)
        {
            var rm = new ResponseModel();
            try
            {
                // Get Client
                Client client = await _clientRepository.Get(client_id);

                // Set new data
                if (client != null)
                {
                    client.address = model.address;
                    client.email = model.email;
                    client.phone = model.phone;

                    //Update and savechanges
                    await _clientRepository.Update(client);
                    await _unit.SaveChanges();
                    rm.response = true;
                    rm.message = "Operación realizada correctamente";
                    rm.result = "Datos actualizados con éxito";

                    // Send Email to confirmation proccess
                    MailVM mail = new MailVM();
                    var rmSendMail = new ResponseModel();
                    mail.destinations.Add(client.email);
                    mail.subject = string.Format("Actualización de datos en  {0}", Constants.COMPANY_NAME_APP);
                    mail.message = string.Format(
                        Constants.MAIL_MESSAGE_UPDATE_CLIENT,
                        Constants.COMPANY_URI_APP,
                        Constants.COMPANY_NAME
                        );
                    rmSendMail = Mail.Send(mail);
                    if(!rmSendMail.response)
                    {
                        rm.message = "Ocurrió un problema al tratar de enviar el correo";
                    }
                }
                else
                {
                    rm.result = "El cliente no existe";
                }
            }
            catch (Exception e)
            {
                _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                _logger.LogError(e, e.Message);
            }
            return rm;
        }
    }
}
