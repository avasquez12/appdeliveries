﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using APP_COMIDAS.Services.Deal;
using APP_COMIDAS.ViewModels.Deal;
using APP_COMIDAS.ViewModels.Deal.Clients;
using FoodClasses.Helpers.Assistants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace APP_COMIDAS.Controllers.Deal
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class ClientsController : ControllerBase
    {
        private readonly IClientService _clientService;
        public ClientsController(IClientService clientService)
        {
            _clientService = clientService;
        }

        /// <summary>
        /// Create a client
        /// </summary>
        /// <param name="model"></param>
        /// /// <remarks>
        /// Sample request:
        ///
        ///     POST /CreateClient
        ///     {
	    ///        "document_type_id": 1, --> Optional
	    ///        "document": 123456789, --> Optional
        ///        "name": "Lorem Ipsum",
        ///        "lastname": "Lorem Ipsum",
        ///        "address": "Av. 80 Diagonal 45",
        ///        "phone": "987654321",
        ///        "email": "a@dominio.com",
        ///        "password": "*******"
        ///     }
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">La petición fué exitosa</response>
        /// <response code="204">La petición fue correcta pero no produjo ningún resultado</response>
        /// <response code="400">La petición no es correcta</response>
        /// <response code="401">No tiene autorización para realizar la petición</response>
        [Produces("application/json", Type = typeof(ResponseModel))]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(200)]
        [ProducesResponseType(204)]
        [HttpPost("CreateClient")]
        public async Task<IActionResult> CreateClient([FromBody]AddClientFromSelfVM model)
        {
            var rm = await _clientService.RegisterClientFromSelf(model);
            return Ok(rm);
        }

        /// <summary>
        /// Update a client
        /// </summary>
        /// <param name="model"></param>
        /// /// <remarks>
        /// Sample request:
        ///
        ///     POST /UpdateClient
        ///     {
	    ///        "address": Registered Address,
	    ///        "phone": Registered Phone number,
        ///        "email": Registered Email
        ///     }
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">La petición fué exitosa</response>
        /// <response code="204">La petición fue correcta pero no produjo ningún resultado</response>
        /// <response code="400">La petición no es correcta</response>
        /// <response code="401">No tiene autorización para realizar la petición</response>
        [Produces("application/json", Type = typeof(ResponseModel))]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(200)]
        [ProducesResponseType(204)]
        [HttpPut("UpdateClient")]
        public async Task<IActionResult> UpdateClient([FromBody]UpdateClientFromSelfVM model)
        {
            //var client = HttpContext.User.Identity as ClaimsIdentity;
            var rm = await _clientService.UpdateClientFromSelf(model, 1);
            return Ok(rm);
        }
    }
}