﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP_COMIDAS.Services.Deal;
using FoodClasses.Helpers;
using FoodClasses.Helpers.Assistants;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace APP_COMIDAS.Controllers.Deal
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly ICategoryService _categoryService;
        public CategoriesController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }


        [HttpPost("AddCategory")]
        public async Task<IActionResult> AddCategory([FromForm] AddCategoryVM model)
        {
            var rm = await _categoryService.AddCategory(model);
            return Ok(rm);
        }

        /// <summary>
        /// Get category
        /// </summary>
        /// <param name="category_id"></param>
        /// /// <remarks>
        /// Sample request:
        ///
        ///     GET /GetCategory{category_id}
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">La petición fué exitosa</response>
        /// <response code="204">La petición fue correcta pero no produjo ningún resultado</response>
        /// <response code="400">La petición no es correcta</response>
        /// <response code="401">No tiene autorización para realizar la petición</response>
        [Produces("application/json", Type = typeof(ResponseModel))]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(200)]
        [ProducesResponseType(204)]
        [HttpGet("GetCategory/{category_id}")]
        public async Task<IActionResult> Get(int category_id)
        {
            var rm = await _categoryService.GetCategory(category_id);
            return Ok(rm);
        }

        /// <summary>
        /// GetAll categories
        /// </summary>
        /// /// <remarks>
        /// Sample request:
        ///
        ///     GET /GetAllCategories
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">La petición fué exitosa</response>
        /// <response code="204">La petición fue correcta pero no produjo ningún resultado</response>
        /// <response code="400">La petición no es correcta</response>
        /// <response code="401">No tiene autorización para realizar la petición</response>
        [Produces("application/json", Type = typeof(ResponseModel))]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(200)]
        [ProducesResponseType(204)]
        [HttpGet("GetAllCategory")]
        public async Task<IActionResult> GetAll()
        {
            var rm = await _categoryService.GetAllCategories();
            return Ok(rm);
        }
    }
}