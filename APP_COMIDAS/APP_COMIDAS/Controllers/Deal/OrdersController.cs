﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using APP_COMIDAS.Services.Deal;
using APP_COMIDAS.ViewModels.Deal;
using FoodClasses.Helpers.Assistants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace APP_COMIDAS.Controllers.Deal
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderService _orderService;
        public OrdersController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        /// <summary>
        /// Save an order
        /// </summary>
        /// <param name="model"></param>
        /// /// <remarks>
        /// Sample request:
        ///
        ///     POST /AddOrder
        ///     {
        ///        "client_id": Registered client ID,
	    ///        "way_to_pay_id": Registered WayToPay ID,
	    ///        "OrderDetail": [
		///             {
		///                 "product_id": Registered Product ID,
		///                 "quantity": Quantity INT
        ///             },
		///             {
		///        	        "product_id": Registered Product ID,
		///	                "quantity": Quantity INT
		///              }
	    ///         ]
        ///     }
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">La petición fué exitosa</response>
        /// <response code="204">La petición fue correcta pero no produjo ningún resultado</response>
        /// <response code="400">La petición no es correcta</response>
        /// <response code="401">No tiene autorización para realizar la petición</response>
        [Produces("application/json", Type = typeof(ResponseModel))]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(200)]
        [ProducesResponseType(204)]
        [HttpPost("AddOrder")]
        public async Task<IActionResult> Post([FromBody]AddOrderVM model)
        {
            var rm = await _orderService.RegisterOrder(model);
            return Ok(rm);
        }

        /// <summary>
        /// Get Orders by Client
        /// </summary>
        /// <param name="client_id"></param>
        /// /// <remarks>
        /// Sample request:
        ///
        ///     GET /GetOrders/{client_id}
        ///     Registered client ID
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">La petición fué exitosa</response>
        /// <response code="204">La petición fue correcta pero no produjo ningún resultado</response>
        /// <response code="400">La petición no es correcta</response>
        /// <response code="401">No tiene autorización para realizar la petición</response>
        [Produces("application/json", Type = typeof(ResponseModel))]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(200)]
        [ProducesResponseType(204)]
        [HttpGet("GetOrders/{client_id}")]
        public async Task<IActionResult> GetOrdersByClient(int client_id)
        {
            //var client = HttpContext.User.Identity as ClaimsIdentity;
            //var rm = await _orderService.GetOrderByClient(int.Parse(client.Name));
            var rm = await _orderService.GetOrderByClient(client_id);
            return Ok(rm);
        }
    }
}