﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP_COMIDAS.Services.Deal;
using FoodClasses.Helpers.Assistants;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace APP_COMIDAS.Controllers.Deal
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompaniesController : ControllerBase
    {
        private readonly ICompanyService _companyService;
        public CompaniesController(ICompanyService companyService)
        {
            _companyService = companyService;
        }

        /// <summary>
        /// Obtain company registered data
        /// </summary>
        /// <param name="id"></param>
        /// /// <remarks>
        /// Sample request:
        ///
        ///     GET /GetCompany/{id}
        ///     {
        ///        "id": "company id"
        ///     }
        ///
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">La petición fué exitosa</response>
        /// <response code="204">La petición fue correcta pero no produjo ningún resultado</response>
        /// <response code="400">La petición no es correcta</response>
        /// <response code="401">No tiene autorización para realizar la petición</response>
        [Produces("application/json", Type = typeof(ResponseModel))]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(200)]
        [ProducesResponseType(204)]
        [HttpGet("GetCompany/{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var rm = await _companyService.Get(id);
            return Ok(rm);
        }
    }
}