﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP_COMIDAS.Services.Security;
using APP_COMIDAS.ViewModels.Security;
using FoodClasses.Helpers.Assistants;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace APP_COMIDAS.Controllers.Security
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IAuthenticationClientService _authService;
        public AuthenticationController(IAuthenticationClientService authService)
        {
            _authService = authService;
        }

        /// <summary>
        /// Obtain a security token to use the exposed services
        /// </summary>
        /// <param name="credentials"></param>
        /// /// <remarks>
        /// Sample request:
        ///
        ///     POST /PostLogin
        ///     {
        ///        "email": "registered email",
        ///        "password": "********"
        ///     }
        ///
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">La petición fué exitosa</response>
        /// <response code="204">La petición fue correcta pero no produjo ningún resultado</response>
        /// <response code="400">La petición no es correcta</response>
        /// <response code="401">No tiene autorización para realizar la petición</response>
        [HttpPost]
        [Produces("application/json", Type = typeof(ResponseModel))]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(200)]
        [ProducesResponseType(204)]
        [Route("Login")]
        public async Task<IActionResult> PostLogin([FromBody] LoginVM credentials)
        {
            var rm = await _authService.Login(credentials);
            return Ok(rm);
        }

        /// <summary>
        /// Obtain an actualized token based on previous token to use the exposed services
        /// </summary>
        /// <param name="refresh"></param>
        /// /// <remarks>
        /// Sample request:
        ///
        ///     POST /PostRefresh
        ///     {
        ///        "refresh": "previous token"
        ///     }
        ///
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">La petición fué exitosa</response>
        /// <response code="204">La petición fue correcta pero no produjo ningún resultado</response>
        /// <response code="400">La petición no es correcta</response>
        /// <response code="401">No tiene autorización para realizar la petición</response>
        [HttpPost]
        [Produces("application/json", Type = typeof(ResponseModel))]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(200)]
        [ProducesResponseType(204)]
        [HttpPost]
        [Route("Refresh/{refresh}")]
        public async Task<IActionResult> PostRefresh(string refresh)
        {
            var rm = await _authService.Refresh(refresh);
            return Ok(rm);
        }

    }
}