﻿using FoodClasses.Models.Entities.Deal;
using FoodClasses.Models.Entities.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP_COMIDAS.ViewModels.Security
{
    public class AuthenticationClientVM
    {
        public Client Client { get; set; }
        public string refreshToken { get; set; }
        public string token { get; set; }
        public int client_id { get; set; }
    }
}
