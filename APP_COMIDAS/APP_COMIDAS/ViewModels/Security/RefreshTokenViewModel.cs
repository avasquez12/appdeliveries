﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP_COMIDAS.ViewModels.Security
{
    public class RefreshTokenViewModel
    {
        public string token { get; set; }
        public string refresh { get; set; }
    }
}
