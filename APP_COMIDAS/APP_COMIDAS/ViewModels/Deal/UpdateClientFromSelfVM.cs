﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP_COMIDAS.ViewModels.Deal
{
    public class UpdateClientFromSelfVM
    {
        public string address { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
    }
}
