﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP_COMIDAS.ViewModels.Deal.Products
{
    public class GetAllProductByCategoryVM
    {
        public int id { get; set; }
        public string name { get; set; }
        public double price { get; set; }
        public int stock { get; set; }
        public string url_file { get; set; }
    }
}
