﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP_COMIDAS.ViewModels.Deal
{
    public class GetOrderByClientVM
    {
        public string way_to_pay { get; set; }
        public IEnumerable<GetOrderDetail> details { get; set; }
        public string estado_pedido { get; set; }
        public double total { get; set; }

        public class GetOrderDetail
        {
            public string product { get; set; }
            public int quantity { get; set; }
            public double price { get; set; }
        }
    }
}
