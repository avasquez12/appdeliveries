﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP_COMIDAS.ViewModels.Deal.Categories
{
    public class GetCategoryVM
    {
        public string name { get; set; }
        public string url_file { get; set; }
    }
}
