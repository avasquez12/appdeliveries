﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP_COMIDAS.ViewModels.Deal
{
    public class GetCompanyVM
    {
        public string company_name { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string cell_phone { get; set; }
        public bool confirmation_whatsapp { get; set; }
        public string email { get; set; }
        public string web_page { get; set; }
        public string opening_hours { get; set; }
    }
}
