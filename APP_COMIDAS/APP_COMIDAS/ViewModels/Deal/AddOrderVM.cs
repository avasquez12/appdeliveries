﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP_COMIDAS.ViewModels.Deal
{
    public class AddOrderVM
    {
        public int client_id { get; set; }
        public int way_to_pay_id { get; set; }
        public IEnumerable<AddOrderDetail> OrderDetail { get; set; }

        public class AddOrderDetail
        {
            public int product_id { get; set; }
            public int quantity { get; set; }
        }
    }
}
