﻿using FoodClasses.Models;
using FoodClasses.Models.Entities.Deal;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP_COMIDAS.Repositories.Deal
{
    // Interface
    public interface IClientRepository : IRepository<Client>
    {
        Task<string> DeleteClient(int id);
    }

    // Class
    public class ClientRepository : IClientRepository
    {
        // Context
        private readonly FoodContext _context;

        public ClientRepository(FoodContext context)
        {
            _context = context;
        }


        public async Task<Client> Add(Client element)
        {
            await _context.Client.AddAsync(element);
            return element;
        }

        public async Task<string> DeleteClient(int id)
        {
            var element = await _context.Client.FindAsync(id);
            element.state = !element.state;
            _context.Client.Update(element);
            return $"El cliente {element.completeName} ha sido inactivado exitosamente";
        }

        public async Task<Client> Get(int id)
        {
            return await _context.Client
                .Include(x => x.DocumentType)
                .Where(x => x.id == id)
                .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Client>> GetAll()
        {
            return await _context.Client
                .Include(x => x.DocumentType)
                .ToListAsync();
        }

        public Task<Client> Update(Client element)
        {
            _context.Client.Update(element);
            return Task.FromResult(element);
        }
    }
}
