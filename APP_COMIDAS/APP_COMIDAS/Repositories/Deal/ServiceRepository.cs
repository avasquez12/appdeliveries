﻿using FoodClasses.Models;
using FoodClasses.Models.Entities.Deal;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP_COMIDAS.Repositories.Deal
{
    // Interface
    public interface IServiceRepository : IRepository<Service>
    {
        Task<string> DeleteService(int id);
        Task<IEnumerable<Service>> GetAllServiceByCategory(int category_id, int paginationNumber);
    }

    // Class
    public class ServiceRepository : IServiceRepository
    {
        // Context
        private readonly FoodContext _context;

        public ServiceRepository(FoodContext context)
        {
            _context = context;
        }


        public async Task<Service> Add(Service element)
        {
            await _context.Service.AddAsync(element);
            return element;
        }

        public async Task<string> DeleteService(int id)
        {
            var element = await _context.Service.FindAsync(id);
            element.state = !element.state;
            _context.Service.Update(element);
            return $"El servicio {element.name} ha sido inactivado exitosamente";
        }

        public async Task<Service> Get(int id)
        {
            return await _context.Service
                .Include(x => x.Category)
                .Where(x => x.id == id)
                .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Service>> GetAll()
        {
            return await _context.Service
                .Include(x => x.Category)
                .ToListAsync();
        }

        public async Task<IEnumerable<Service>> GetAllServiceByCategory(int category_id, int paginationNumber)
        {
            var total = await _context.Service
                .Include(x => x.Category)
                .Where(x => x.category_id == category_id)
                .ToListAsync();
            
            int page = total.Count() / 10;
            if(page >= 1)
            {
                if(paginationNumber <= page)
                {
                    int until = 10;
                    return total.Take(10).Skip(until*paginationNumber);
                }
                else
                {
                    return new List<Service>();
                }
            }
            else
            {
                return total;
            }
        }

        public Task<Service> Update(Service element)
        {
            _context.Service.Update(element);
            return Task.FromResult(element);
        }
    }
}
