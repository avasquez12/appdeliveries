﻿using FoodClasses.Models;
using FoodClasses.Models.Entities.Deal;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP_COMIDAS.Repositories.Deal
{
    // Interface
    public interface IOrderStateRepository : IRepository<OrderState>
    {
        Task<string> DeleteOrderState(int id);
    }

    // Class
    public class OrderStateRepository : IOrderStateRepository
    {
        // Context
        private readonly FoodContext _context;

        public OrderStateRepository(FoodContext context)
        {
            _context = context;
        }


        public async Task<OrderState> Add(OrderState element)
        {
            await _context.OrderState.AddAsync(element);
            return element;
        }

        public async Task<string> DeleteOrderState(int id)
        {
            var element = await _context.OrderState.FindAsync(id);
            element.state = !element.state;
            _context.OrderState.Update(element);
            return $"El estado del pedido {element.description} ha sido inactivado exitosamente";
        }

        public async Task<OrderState> Get(int id)
        {
            var element = await _context.OrderState.FindAsync(id);
            return element;
        }

        public async Task<IEnumerable<OrderState>> GetAll()
        {
            return await _context.OrderState.ToListAsync();
        }

        public Task<OrderState> Update(OrderState element)
        {
            _context.OrderState.Update(element);
            return Task.FromResult(element);
        }
    }
}
