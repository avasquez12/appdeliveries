﻿using FoodClasses.Models;
using FoodClasses.Models.Entities.Deal;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP_COMIDAS.Repositories.Deal
{
    // Interface
    public interface IGalleryRepository : IRepository<Gallery>
    {
        Task<string> DeletePhoto(int id);
        Task<IEnumerable<Gallery>> GetAllGalleryFilterByIdProduct(IEnumerable<int> products);
    }

    // Class
    public class GalleryRepository : IGalleryRepository
    {
        // Context
        private readonly FoodContext _context;

        public GalleryRepository(FoodContext context)
        {
            _context = context;
        }


        public async Task<Gallery> Add(Gallery element)
        {
            await _context.Gallery.AddAsync(element);
            return element;
        }

        public async Task<string> DeletePhoto(int id)
        {
            var element = await _context.Gallery.FindAsync(id);
            _context.Gallery.Remove(element);
            return $"La fotografía ha sido eliminada exitosamente";
        }

        public async Task<Gallery> Get(int id)
        {
            var element = await _context.Gallery.FindAsync(id);
            return element;
        }

        public async Task<IEnumerable<Gallery>> GetAll()
        {
            return await _context.Gallery.ToListAsync();
        }

        public async Task<IEnumerable<Gallery>> GetAllGalleryFilterByIdProduct(IEnumerable<int> products)
        {
            var galleries = _context.Gallery.Where(x => x.product_id != 0)
                .Include(x => x.Product);

            return await galleries.Where(x => products.Contains(x.product_id)).ToListAsync();
        }

        public Task<Gallery> Update(Gallery element)
        {
            _context.Gallery.Update(element);
            return Task.FromResult(element);
        }
    }
}
