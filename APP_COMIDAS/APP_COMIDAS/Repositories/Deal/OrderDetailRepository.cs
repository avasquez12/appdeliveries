﻿using FoodClasses.Models;
using FoodClasses.Models.Entities.Deal;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP_COMIDAS.Repositories.Deal
{
    // Interface
    public interface IOrderDetailRepository : IRepository<OrderDetail>
    {
        Task<IEnumerable<OrderDetail>> GetAllOrderDetailByOrder(int order_id);
        Task<IEnumerable<OrderDetail>> GetAllOrderDetailByProduct(int product_id);
        Task<IEnumerable<OrderDetail>> GetAllOrderDetailByService(int service_id);
    }

    // Class
    public class OrderDetailRepository : IOrderDetailRepository
    {
        // Context
        private readonly FoodContext _context;

        public OrderDetailRepository(FoodContext context)
        {
            _context = context;
        }


        public async Task<OrderDetail> Add(OrderDetail element)
        {
            await _context.OrderDetail.AddAsync(element);
            return element;
        }

        public Task<OrderDetail> Get(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<OrderDetail>> GetAll()
        {
            return await _context.OrderDetail
                .Include(x => x.Product)
                .Include(x => x.Order)
                .ToListAsync();
        }

        public async Task<IEnumerable<OrderDetail>> GetAllOrderDetailByProduct(int product_id)
        {
            return await _context.OrderDetail
                .Include(x => x.Product)
                .Include(x => x.Order)
                .Where(x => x.product_id == product_id)
                .ToListAsync();
        }

        public async Task<IEnumerable<OrderDetail>> GetAllOrderDetailByOrder(int order_id)
        {
            return await _context.OrderDetail
                .Include(x => x.Product)
                .Include(x => x.Order)
                .Where(x => x.order_id == order_id)
                .ToListAsync();
        }

        public async Task<IEnumerable<OrderDetail>> GetAllOrderDetailByService(int service_id)
        {
            return await _context.OrderDetail
                .Include(x => x.Service)
                .Include(x => x.Order)
                .Where(x => x.service_id == service_id)
                .ToListAsync();
        }

        public Task<OrderDetail> Update(OrderDetail element)
        {
            throw new NotImplementedException();
        }
    }
}
