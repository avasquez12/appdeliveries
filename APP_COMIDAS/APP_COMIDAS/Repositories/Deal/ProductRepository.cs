﻿using FoodClasses.Models;
using FoodClasses.Models.Entities.Deal;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP_COMIDAS.Repositories.Deal
{
    // Interface
    public interface IProductRepository : IRepository<Product>
    {
        Task<string> DeleteProduct(int id);
        Task<IEnumerable<Product>> GetAllProductByCategory(int category_id, int paginationNumber);
    }

    // Class
    public class ProductRepository : IProductRepository
    {
        // Context
        private readonly FoodContext _context;

        public ProductRepository(FoodContext context)
        {
            _context = context;
        }


        public async Task<Product> Add(Product element)
        {
            await _context.Product.AddAsync(element);
            return element;
        }

        public async Task<string> DeleteProduct(int id)
        {
            var element = await _context.Product.FindAsync(id);
            element.state = !element.state;
            _context.Product.Update(element);
            return $"El producto {element.name} ha sido inactivado exitosamente";
        }

        public async Task<Product> Get(int id)
        {
            return await _context.Product
                .Include(x => x.Category)
                .Where(x => x.id == id)
                .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Product>> GetAll()
        {
            return await _context.Product
                .Include(x => x.Category)
                .ToListAsync();
        }

        public async Task<IEnumerable<Product>> GetAllProductByCategory(int category_id, int paginationNumber)
        {
            var total = await _context.Product
                .Include(x => x.Category)
                .Where(x => x.category_id == category_id)
                .ToListAsync();
            
            int page = total.Count() / 10;
            if(page >= 1)
            {
                if(paginationNumber <= page)
                {
                    int until = 10;
                    return total.Take(10).Skip(until*paginationNumber);
                }
                else
                {
                    return new List<Product>();
                }
            }
            else
            {
                return total;
            }
        }

        public Task<Product> Update(Product element)
        {
            _context.Product.Update(element);
            return Task.FromResult(element);
        }
    }
}
