﻿using FoodClasses.Models;
using FoodClasses.Models.Entities.Deal;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP_COMIDAS.Repositories.Deal
{
    // Interface
    public interface IWayToPayRepository : IRepository<WayToPay>
    {
        Task<string> DeleteWayToPay(int id);
    }

    // Class
    public class WayToPayRepository : IWayToPayRepository
    {
        // Context
        private readonly FoodContext _context;

        public WayToPayRepository(FoodContext context)
        {
            _context = context;
        }


        public async Task<WayToPay> Add(WayToPay element)
        {
            await _context.WayToPay.AddAsync(element);
            return element;
        }

        public async Task<string> DeleteWayToPay(int id)
        {
            var element = await _context.WayToPay.FindAsync(id);
            element.state = !element.state;
            _context.WayToPay.Update(element);
            return $"La forma de pago {element.description} ha sido inactivada exitosamente";
        }

        public async Task<WayToPay> Get(int id)
        {
            var element = await _context.WayToPay.FindAsync(id);
            return element;
        }

        public async Task<IEnumerable<WayToPay>> GetAll()
        {
            return await _context.WayToPay.ToListAsync();
        }

        public Task<WayToPay> Update(WayToPay element)
        {
            _context.WayToPay.Update(element);
            return Task.FromResult(element);
        }
    }
}
