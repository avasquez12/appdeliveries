﻿using FoodClasses.Models;
using FoodClasses.Models.Entities.Deal;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP_COMIDAS.Repositories.Deal
{
    // Interface
    public interface ICategoryRepository : IRepository<Category>
    {
        Task<string> DeleteCategory(int id);
    }

    // Class
    public class CategoryRepository : ICategoryRepository
    {
        // Context
        private readonly FoodContext _context;

        public CategoryRepository(FoodContext context)
        {
            _context = context;
        }


        public async Task<Category> Add(Category element)
        {
            await _context.Category.AddAsync(element);
            return element;
        }

        public async Task<string> DeleteCategory(int id)
        {
            var element = await _context.Category.FindAsync(id);
            element.state = !element.state;
            _context.Category.Update(element);
            return $"La categoría {element.name} ha sido eliminada exitosamente";
        }

        public async Task<Category> Get(int id)
        {
            var element = await _context.Category.FindAsync(id);
            return element;
        }

        public async Task<IEnumerable<Category>> GetAll()
        {
            return await _context.Category.ToListAsync();
        }

        public Task<Category> Update(Category element)
        {
            _context.Category.Update(element);
            return Task.FromResult(element);
        }
    }
}
