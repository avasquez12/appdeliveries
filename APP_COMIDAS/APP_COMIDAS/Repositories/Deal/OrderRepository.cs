﻿using FoodClasses.Models;
using FoodClasses.Models.Entities.Deal;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP_COMIDAS.Repositories.Deal
{
    // Interface
    public interface IOrderRepository : IRepository<Order>
    {
        Task<IEnumerable<Order>> GetAllOrderByClient(int client_id);
        Task<IEnumerable<Order>> GetAllOrderByWayToPay(int waytopay_id);
    }

    // Class
    public class OrderRepository : IOrderRepository
    {
        // Context
        private readonly FoodContext _context;

        public OrderRepository(FoodContext context)
        {
            _context = context;
        }


        public async Task<Order> Add(Order element)
        {
            await _context.Order.AddAsync(element);
            return element;
        }

        public async Task<Order> Get(int id)
        {
            return await _context.Order
                .Include(x => x.Client)
                .Include(x => x.WayToPay)
                .Where(x => x.id == id)
                .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Order>> GetAll()
        {
            return await _context.Order
                .Include(x => x.Client)
                .Include(x => x.WayToPay)
                .ToListAsync();
        }

        public async Task<IEnumerable<Order>> GetAllOrderByClient(int client_id)
        {
            return await _context.Order
                .Include(x => x.Client)
                .Include(x => x.WayToPay)
                .Include(x => x.OrderState)
                .Include(x => x.OrdersDetail)
                .ThenInclude(x => x.Product)
                .Where(x => x.client_id == client_id)
                .ToListAsync();
        }

        public async Task<IEnumerable<Order>> GetAllOrderByWayToPay(int waytopay_id)
        {
            return await _context.Order
                .Include(x => x.Client)
                .Include(x => x.WayToPay)
                .Where(x => x.way_to_pay_id == waytopay_id)
                .ToListAsync();
        }

        public Task<Order> Update(Order element)
        {
            _context.Order.Update(element);
            return Task.FromResult(element);
        }
    }
}
