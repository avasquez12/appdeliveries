﻿using FoodClasses.Models;
using FoodClasses.Models.Entities.Deal;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP_COMIDAS.Repositories.Deal
{
    // Interface
    public interface ICompanyRepository : IRepository<Company>
    {
    }

    // Class
    public class CompanyRepository : ICompanyRepository
    {
        // Context
        private readonly FoodContext _context;

        public CompanyRepository(FoodContext context)
        {
            _context = context;
        }


        public Task<Company> Add(Company element)
        {
            throw new NotImplementedException();
        }

        public async Task<Company> Get(int id)
        {
            return await _context.Company
                .Include(x => x.DocumentType)
                .Where(x => x.id == id)
                .FirstOrDefaultAsync();
        }

        public Task<IEnumerable<Company>> GetAll()
        {
            throw new NotImplementedException();
        }

        public Task<Company> Update(Company element)
        {
            throw new NotImplementedException();
        }
    }
}
