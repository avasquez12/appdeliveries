﻿using FoodClasses.Models;
using FoodClasses.Models.Entities.Deal;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP_COMIDAS.Repositories.Deal
{
    // Interface
    public interface IDocumentTypeRepository : IRepository<DocumentType>
    {
    }

    // Class
    public class DocumentTypeRepository : IDocumentTypeRepository
    {
        // Context
        private readonly FoodContext _context;

        public DocumentTypeRepository(FoodContext context)
        {
            _context = context;
        }


        public async Task<DocumentType> Add(DocumentType element)
        {
            await _context.DocumentType.AddAsync(element);
            return element;
        }

        public async Task<DocumentType> Get(int id)
        {
            var element = await _context.DocumentType.FindAsync(id);
            return element;
        }

        public async Task<IEnumerable<DocumentType>> GetAll()
        {
            return await _context.DocumentType.ToListAsync();
        }

        public Task<DocumentType> Update(DocumentType element)
        {
            _context.DocumentType.Update(element);
            return Task.FromResult(element);
        }
    }
}
