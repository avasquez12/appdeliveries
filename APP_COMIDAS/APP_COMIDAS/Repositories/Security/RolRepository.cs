﻿using FoodClasses.Helpers.Assistants;
using FoodClasses.Models;
using FoodClasses.Models.Entities.Security;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP_COMIDAS.Repositories.Security
{
    // Interface
    public interface IRolRepository : IRepository<Rol>
    {
        Task DeleteRoles(int id);
    }


    // Class
    public class RolRepository : IRolRepository
    {
        // Context
        private readonly FoodContext _context;

        public RolRepository(FoodContext context)
        {
            _context = context;
        }

        public Task<Rol> Add(Rol element)
        {
            throw new NotImplementedException();
        }

        public async Task DeleteRoles(int id)
        {
            var delete = await _context.Rol.FindAsync(id);
            _context.Rol.Remove(delete);
        }

        public async Task<Rol> Get(int id)
        {
            return await _context.Rol.FindAsync(id);
        }

        public async Task<IEnumerable<Rol>> GetAll()
        {
            return await _context.Rol.Where(x => x.id != Constants.ROL_SUPER).ToListAsync();
        }

        public Task<Rol> Update(Rol element)
        {
            throw new NotImplementedException();
        }
    }
}
