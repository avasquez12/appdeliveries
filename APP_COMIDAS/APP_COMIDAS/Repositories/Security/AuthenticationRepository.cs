﻿using FoodClasses.Models;
using FoodClasses.Models.Entities.Security;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP_COMIDAS.Repositories.Security
{
    // Interface
    public interface IAuthenticationRepository : IRepository<Authentication>
    { 
    }

    // Class
    public class AuthenticationRepository : IAuthenticationRepository
    {
        // Context
        private readonly FoodContext _context;

        public AuthenticationRepository(FoodContext context)
        {
            _context = context;
        }


        public async Task<Authentication> Add(Authentication element)
        {
            await _context.AddAsync(element);
            return element;
        }

        public async Task<Authentication> Get(int id)
        {
            var element = await _context.Authentication.FindAsync(id);
            return element;
        }

        public async Task<IEnumerable<Authentication>> GetAll()
        {
            return await _context.Authentication.ToListAsync();
        }

        public Task<Authentication> Update(Authentication element)
        {
            _context.Authentication.Update(element);
            return Task.FromResult(element);
        }
    }
}
