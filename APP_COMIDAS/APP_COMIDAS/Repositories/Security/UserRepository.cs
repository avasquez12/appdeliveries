﻿using APP_COMIDAS.Helpers.Assistants;
using FoodClasses.Models;
using FoodClasses.Models.Entities.Security;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP_COMIDAS.Repositories.Security
{
    // Interface
    public interface IUserRepository : IRepository<User>
    {
        Task<string> ChangeState(int id);
    }

    // Class
    public class UserRepository : IUserRepository
    {
        // Context
        private readonly FoodContext _context;

        public UserRepository(FoodContext context)
        {
            _context = context;
        }


        public async Task<User> Add(User element)
        {
            await _context.User.AddAsync(element);
            return element;
        }

        public async Task<string> ChangeState(int id)
        {
            var element = await _context.User.FindAsync(id);
            element.state = !element.state;
            _context.User.Update(element);
            return $"El usuario {element.completeName} ha sido eliminado exitosamente";
        }

        public async Task<User> Get(int id)
        {
            return await _context.User.Include(x => x.Rol)
                .Where(x => x.id == id).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            return await _context.User
                .Include(x => x.Rol).ToListAsync();
        }

        public Task<User> Update(User element)
        {
            _context.User.Update(element);
            return Task.FromResult(element.WithoutPassword());
        }
    }
}
