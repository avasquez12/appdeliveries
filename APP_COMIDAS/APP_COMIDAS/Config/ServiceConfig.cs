﻿using APP_COMIDAS.Repositories;
using APP_COMIDAS.Repositories.Deal;
using APP_COMIDAS.Repositories.Security;
using APP_COMIDAS.Services.Deal;
using APP_COMIDAS.Services.Security;
using FoodClasses.Models;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APP_COMIDAS.Config
{
    public static class ServiceConfig
    {
        public static IServiceCollection AddRegistration(this IServiceCollection services)
        {
            AddRegisterServices(services);
            AddRegisterRepositories(services);
            return services;
        }

        private static IServiceCollection AddRegisterServices(IServiceCollection services)
        {
            services.AddTransient<IAuthenticationClientService, AuthenticationClientService>();
            services.AddTransient<ICompanyService, CompanyService>();
            services.AddTransient<IOrderService, OrderService>();
            services.AddTransient<IClientService, ClientService>();
            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<IServiceService, ServiceService>();

            return services;
        }

        private static IServiceCollection AddRegisterRepositories(IServiceCollection services)
        {
            // Security
            services.AddTransient<IAuthenticationRepository, AuthenticationRepository>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IRolRepository, RolRepository>();

            // Deal
            services.AddTransient<ICategoryRepository, CategoryRepository>();
            services.AddTransient<IClientRepository, ClientRepository>();
            services.AddTransient<IDocumentTypeRepository, DocumentTypeRepository>();
            services.AddTransient<IGalleryRepository, GalleryRepository>();
            services.AddTransient<IOrderRepository, OrderRepository>();
            services.AddTransient<IOrderDetailRepository, OrderDetailRepository>();
            services.AddTransient<IProductRepository, ProductRepository>();
            services.AddTransient<IWayToPayRepository, WayToPayRepository>();
            services.AddTransient<ICompanyRepository, CompanyRepository>();
            services.AddTransient<IOrderStateRepository, OrderStateRepository>();
            services.AddTransient<IServiceRepository, ServiceRepository>();

            // Save
            services.AddTransient<IUnitOfWork, UnitOfWork>();

            return services;
        }
    }
}
