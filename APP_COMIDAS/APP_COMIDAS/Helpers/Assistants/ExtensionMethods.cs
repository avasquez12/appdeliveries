﻿using APP_COMIDAS.ViewModels.Deal;
using FoodClasses.Models.Entities.Deal;
using FoodClasses.Models.Entities.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace APP_COMIDAS.Helpers.Assistants
{
    public static class ExtensionMethods
    {
        public static string GetClaim(this ClaimsIdentity identity, string paramtroBusqueda)
        {
            return identity.Claims.Where(c => c.Type == paramtroBusqueda)
                                .Select(x => x.Value).FirstOrDefault();
        }

        public static IEnumerable<User> WithoutPasswords(this IEnumerable<User> users)
        {
            return users.Select(x => x.WithoutPassword());
        }

        public static User WithoutPassword(this User user)
        {
            user.password = null;
            return user;
        }

        // Calculate total price by OrderDetail
        // VM to GetTotalPrice Method
        public class GroupingOrderDetailByOrderVM
        {
            public int order_id { get; set; }
            public List<OrderDetail> orderDetails;
        }
        public static double GetTotalPrice(IEnumerable<GroupingOrderDetailByOrderVM> prices)
        {
            double totalPrice = 0.0;

            foreach (var item in prices)
            {
                foreach (var price in item.orderDetails)
                {
                    totalPrice += price.price * (int)price.quantity;
                }
            }

            return totalPrice;
        }
    }
}
