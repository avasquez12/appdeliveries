$(document).ready(function () {
    $("body").on('click', 'button', function () {
        $("#autosave").val(0);        

        // Si el boton no tiene el atributo ajax no hacemos nada
        if ($(this).data('ajax') == undefined) return;

        // El metodo .data identifica la entrada y la castea al valor más correcto
        if ($(this).data('ajax') != true) return;

        var form = $(this).closest("form");
        var buttons = $("button", form);
        var button = $(this);
        var url = form.attr('action');

        if (button.data('confirm') != undefined)
        {
            if (button.data('confirm') == '') {
                if (!confirm('¿Esta seguro de realizar esta acción?')) return false;
            } else {
                if (!confirm(button.data('confirm'))) return false;
            }
        }

        if (button.data('delete') != undefined)
        {
            if (button.data('delete') == true)
            {
                url = button.data('url');
            }
        }else
        {
            if (!form.valid()) {
                return false;
            }
        }

        // Creamos un div que bloqueara todo el formulario
        var block = $('<div class="block-loading"><i class="fa fa-pulse fa-sync-alt"></i></div>');
        form.prepend(block);

        // En caso de que haya habido un mensaje de alerta
        $(".alert", form).remove();

        // Para los formularios que tengan CKupdate
        if (form.hasClass('CKupdate')) CKupdate();

        //Tipos de desplegables cargados en los form
        var generales = $("select").hasClass("select2General");
        var select2Victimas = $("select").hasClass("select2Victimas");
        var ciudades = $("select").hasClass("select2Ajax");
        var ciudadanos = $("select").hasClass("select2AjaxCiudadano");

        //si hay atributo sweet enviamos el ajax normal
        if ($(this).data('sweet') == undefined) {

            form.ajaxSubmit({
                dataType: 'JSON',
                type: 'POST',
                url: url,
                success: function (r) {
                    
                    if (r.response) {
                        if (!button.data('reset') != undefined) {
                            if (button.data('reset')) {
                                form.clearForm();
                                if (generales) {
                                    $("form .select2General").select2("val", "");
                                    $("form .select2General").trigger('change');
                                }
                                if (select2Victimas) {
                                    $("form .select2Victimas").select2("val", "");
                                    $("form .select2Victimas").trigger('change');
                                }
                                if (ciudades) {
                                    $("form .select2Ajax").select2("val", "");
                                    $("form .select2Ajax").trigger('change');
                                }
                                if (ciudadanos) {
                                    $("form .select2AjaxCiudadano").select2("val", "");
                                    $("form .select2AjaxCiudadano").trigger('change');
                                }
                            }
                        }
                        else {
                            form.find('input:file').val('');
                        }
                    }

                    // Mostrar mensaje
                    if (r.message != null) {
                        if (r.message.length > 0) {
                            var css = "";
                            if (r.response) css = "alert-success";
                            else css = "alert-danger";

                            var message = '<div class="alert ' + css + ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + r.message + '</div>';
                            form.prepend(message);
                            var elmnt = document.body;
                            elmnt.scrollTop;
                        }
                    }

                    // Ejecutar funciones
                    if (r.function != null) {
                        setTimeout(r.function, 0);
                    }
                    // Redireccionar
                    if (r.href != null) {
                        if (r.href == 'self') window.location.reload(true);
                        else window.location.href = r.href;
                    }
                    block.remove();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    block.remove();
                    form.prepend('<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + errorThrown + ' | <b>' + textStatus + '</b></div>');
                    if (jqXHR.status === 403) {
                        var texto = jqXHR.responseText.split("|");
                        alert(texto[1]);
                        if (texto[0] === '1') {
                            window.location = '/LoginUsuario/LoginUsuario';
                        } else {
                            if (texto[0] === '0') {
                                window.location = '/LoginCiudadano/LoginCiudadano';
                            }
                            else {
                                window.location = '/LoginUsuarioExterno/LoginUsuarioExterno';
                            }                            
                        }
                    }
                }
            });

        } else {

            var titulo = "";
            if (button.data('sweet') == '') {
                titulo = '¿Esta seguro de realizar esta acción?';
            } else {
                titulo = button.data('sweet');
            }

            swal({
                title: titulo,
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#70CDE8',
                confirmButtonText: 'Si',
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true
            },
                function (isConfirm) {
                    if (isConfirm) {
                        document.getElementById("sweet").value = 1;
                    } else {
                        document.getElementById("sweet").value = 0;
                    }
                    form.ajaxSubmit({
                        dataType: 'JSON',
                        type: 'POST',
                        url: url,
                        success: function (r) {
                            block.remove();
                            if (r.response) {
                                if (!button.data('reset') != undefined) {
                                    if (button.data('reset')) form.reset();
                                }
                                else {
                                    form.find('input:file').val('');
                                }
                            }

                            // Mostrar mensaje
                            if (r.message != null) {
                                if (r.message.length > 0) {
                                    var css = "";
                                    if (r.response) css = "alert-success";
                                    else css = "alert-danger";

                                    var message = '<div class="alert ' + css + ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + r.message + '</div>';
                                    form.prepend(message);
                                }
                            }

                            // Ejecutar funciones
                            if (r.function != null) {
                                setTimeout(r.function, 0);
                            }
                            // Redireccionar
                            if (r.href != null) {
                                if (r.href == 'self') window.location.reload(true);
                                else window.location.href = r.href;
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            block.remove();
                            form.prepend('<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + errorThrown + ' | <b>' + textStatus + '</b></div>');
                            if (jqXHR.status === 403) {
                                var texto = jqXHR.responseText.split("|");
                                alert(texto[1]);
                                if (texto[0] === '1') {
                                    window.location = '/LoginUsuario/LoginUsuario';
                                } else {
                                    window.location = '/LoginCiudadano/LoginCiudadano';
                                }
                            }
                        }
                    });
                });
        }

        return false;
    })
})

jQuery.fn.reset = function () {
    $("input:password,input:file,input:text,textarea", $(this)).val('');
    $("input:checkbox:checked", $(this)).click();
    $("select").each(function () {
        $(this).val($("option:first", $(this)).val());
    })
};