﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BACK_FOOD_MVC.Helpers.Assistants
{
    public class Encrypt
    {
        public static string Encrypt_String(string str)
        {
            string EncrptKey = "Fivetech";
            byte[] byKey = { };
            byte[] IV = { 18, 52, 86, 120, 144, 171, 205, 239 };
            byKey = System.Text.Encoding.UTF8.GetBytes(EncrptKey);
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            byte[] inputByteArray = Encoding.UTF8.GetBytes(str);
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(byKey, IV), CryptoStreamMode.Write);
            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();
            string result = Convert.ToBase64String(ms.ToArray());
            result = result.Replace("/", "-");
            result = result.Replace("+", "@");
            return result;
        }

        public static string Decrypt_String(string str)
        {
            string strreturn = string.Empty;
            try
            {
                //str = str.Replace(" ", "+");
                str = str.Replace("@", "+");
                str = str.Replace("-", "/");
                string DecryptKey = "Fivetech";
                byte[] byKey = { };
                byte[] IV = { 18, 52, 86, 120, 144, 171, 205, 239 };
                byte[] inputByteArray = new byte[str.Length];

                byKey = System.Text.Encoding.UTF8.GetBytes(DecryptKey.Substring(0, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                inputByteArray = Convert.FromBase64String(str);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(byKey, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                strreturn = encoding.GetString(ms.ToArray());

            }
            catch (Exception ex)
            {
                //throw ex;
                ex.Message.ToString();
            }
            return strreturn;
        }
    }
}
