﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BACK_FOOD_MVC.Helpers.Assistants
{
    public class Alerts
    {
        public static string Message(string action = "")
        {
            string msg = "";

            // Build message
            switch (action)
            {
                case "register":
                    msg = "¡El registro ha sido guardado con éxito!";
                    break;

                case "update":
                    msg = "¡El registro ha sido actualizado con éxito!";
                    break;

                case "delete":
                    msg = "¡El registro ha sido eliminado con éxito!";
                    break;

                case "active":
                    msg = "¡El registro ha sido activado con éxito!";
                    break;

                case "errorDelete":
                    msg = "¡El registro no puede ser eliminado!";
                    break;
                case "inactive":
                    msg = "¡El registro ha sido desactivado con éxito!";
                    break;
                default:
                    msg = "El registro no se encuentra en el sistema";
                    break;
            }

            return msg;
        }

        public static string Alert(string type, string msg = "")
        {
            string text = "";

            switch (type)
            {
                case "info":
                    text = "<div class=\"alert alert-dismissible alert-info\">" +
                              "<button type = \"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" +
                              "<h4><i class=\"fa fa-info\" aria-hidden=\"true\"></i> Aviso</h4>" +
                              "<p>" + msg + "</p>" +
                              "</div>";
                    break;

                case "success":
                    text = "<div class=\"alert alert-dismissible alert-success\">" +
                              "<button type = \"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" +
                              "<h4><i class=\"fa fa-check\" aria-hidden=\"true\"></i> Aviso</h4>" +
                              "<p>" + msg + "</p>" +
                              "</div>";
                    break;

                case "warning":
                    text = "<div class=\"alert alert-dismissible alert-warning\">" +
                              "<button type = \"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" +
                              "<h4><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i> Aviso</h4>" +
                              "<p>" + msg + "</p>" +
                              "</div>";
                    break;

                case "danger":
                    text = "<div class=\"alert alert-dismissible alert-danger\">" +
                              "<button type = \"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" +
                              "<h4><i class=\"fa fa-ban\" aria-hidden=\"true\"></i> Aviso</h4>" +
                              "<p>" + msg + "</p>" +
                              "</div>";
                    break;

                default:
                    text = "";
                    break;

            }

            return text;
        }
    }
}
