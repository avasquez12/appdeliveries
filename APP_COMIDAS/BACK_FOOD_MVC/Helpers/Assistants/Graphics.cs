﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BACK_FOOD_MVC.Helpers.Assistants
{
    public class BarGraphic
    {
        // Graphic Y axis
        public string[] labels { get; set; }
        public string label { get; set; }
        public int[] data { get; set; }
        public int size { get; set; }


        public BarGraphic(int size)
        {
            this.size = size;
            this.data = new int[size];
            if(size == 7)
            {
                SetDays();
            }
            else
            {
                this.labels = new string[size];
            }
        }

        private void SetMonths()
        {
            this.labels = new string[] { "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic" };
        }

        public void SetDays()
        {
            this.labels = new string[] { "Lun", "Mar", "Mie", "Jue", "Vie", "Sab", "Dom" };
            //for (int i = 0; i < this.size; i++)
            //{
            //    this.labels[i] = "" + (i + 1);
            //}
        }
    }
}
