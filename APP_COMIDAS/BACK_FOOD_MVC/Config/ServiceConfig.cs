﻿using BACK_FOOD_MVC.Services;
using FoodClasses.Models;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BACK_FOOD_MVC.Config
{
    public static class ServiceConfig
    {
        public static IServiceCollection AddRegistration(this IServiceCollection services)
        {
            AddRegisterServices(services);
            return services;
        }

        private static IServiceCollection AddRegisterServices(IServiceCollection services)
        {
            services.AddTransient<IUserService, UserService>();

            // Save
            services.AddTransient<IUnitOfWork, UnitOfWork>();

            return services;
        }
    }
}
