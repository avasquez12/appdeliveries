﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BACK_FOOD_MVC.ViewModels.Products
{
    public class AddProductVM
    {
        [Display(Name = "Nombre")]
        [Required]
        public string name { get; set; }

        [Display(Name = "Categoría")]
        [Required]
        public int category_id { get; set; }

        [Display(Name = "Stock")]
        [Required]
        public int stock { get; set; }

        [Display(Name = "Stock mínimo")]
        [Required]
        public int stock_min { get; set; }

        [Display(Name = "Precio")]
        [Required]
        public double price { get; set; }

        [Display(Name = "Foto")]
        public IFormFile file { get; set; }
    }
}
