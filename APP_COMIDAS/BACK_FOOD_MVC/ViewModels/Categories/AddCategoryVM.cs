﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BACK_FOOD_MVC.ViewModels.Categories
{
    public class AddCategoryVM
    {
        [Display(Name = "Nombre")]
        [Required]
        public string name { get; set; }
        [Display(Name = "Archivo")]
        [Required]
        public IFormFile file { get; set; }
    }
}
