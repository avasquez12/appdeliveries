﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BACK_FOOD_MVC.ViewModels.Users
{
    public class UserDataVM
    {
        public string NombreCompleto { get; set; }
        public string NombreRol { get; set; }
    }
}
