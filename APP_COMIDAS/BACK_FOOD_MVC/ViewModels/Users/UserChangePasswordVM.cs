﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BACK_FOOD_MVC.ViewModels.Users
{
    public class UserChangePasswordVM
    {
        [Display(Name = "Contraseña anterior")]
        [DataType(DataType.Password)]
        [Required]
        public string old_password { get; set; }

        [Display(Name = "Contraseña nueva")]
        [DataType(DataType.Password)]
        [Required]
        [RegularExpression(@"^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!*@#$%^&+=]).*$",
         ErrorMessage = "La contraseña no cumple con los criterios establecidos")]
        public string new_password { get; set; }

        [Display(Name = "Confirmación de constraseña")]
        [DataType(DataType.Password)]
        [Required]
        [RegularExpression(@"^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!*@#$%^&+=]).*$",
         ErrorMessage = "La contraseña no cumple con los criterios establecidos")]
        public string confirm_password { get; set; }
    }
}
