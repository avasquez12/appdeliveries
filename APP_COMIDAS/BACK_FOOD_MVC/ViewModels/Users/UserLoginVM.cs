﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BACK_FOOD_MVC.ViewModels.Users
{
    public class UserLoginVM
    {
        public string email { get; set; }
        public string password { get; set; }
    }
}
