﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using BACK_FOOD_MVC.Helpers.Assistants;
using FoodClasses.Helpers.Assistants;
using FoodClasses.Models;
using FoodClasses.Models.Entities.Deal;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SmartBreadcrumbs.Attributes;

namespace BACK_FOOD_MVC.Controllers
{
    [Authorize]
    public class OrderStatesController : Controller
    {
        private readonly FoodContext _context;
        private readonly IUnitOfWork _unit;
        private readonly ILogger<OrderStatesController> _logger;

        public OrderStatesController(FoodContext context, IUnitOfWork unit,
            ILogger<OrderStatesController> logger)
        {
            _context = context;
            _unit = unit;
            _logger = logger;
        }


        /// <summary>
        /// Order State's list
        /// </summary>
        /// <param name="type">Alert type to show a message after an action</param>
        /// <param name="message">Message content</param>
        /// <returns></returns>
        [Breadcrumb("Estados")]
        public async Task<IActionResult> Index(string type = "", string message = "")
        {
            ViewBag.msg = Alerts.Alert(type, message);

            var states = await _context.OrderState.ToListAsync();
            return View(states);
        }

        /// <summary>
        /// Method to register a state order
        /// </summary>
        /// <returns></returns>
        [Breadcrumb("Registrar", FromAction = "Index")]
        public ActionResult Register()
        {
            return View();
        }

        /// <summary>
        /// POST Method to register a new category
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> Register(OrderState model)
        {
            var rm = new ResponseModel();

            if (ModelState.IsValid)
            {
                try
                {
                    // Complete model
                    model.state = true;
                    _context.OrderState.Add(model);

                    // Save changes
                    try
                    {
                        await _unit.SaveChanges();
                        rm.SetResponse(true, Alerts.Message("register"));
                        rm.href = Url.Action("Index", "OrderStates", new { type = "success", message = rm.message });
                    }
                    catch (Exception error)
                    {
                        rm.message = error.Message;
                    }
                }
                catch (Exception error)
                {
                    rm.message = error.Message;
                    _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                    _logger.LogError(error, error.Message);
                }
            }
            return Json(rm);
        }

        /// <summary>
        /// Method to update an order state
        /// </summary>
        /// <param name="item">Search state parameter</param>
        /// <returns></returns>
        [Breadcrumb("Actualizar", FromAction = "Index")]
        public async Task<IActionResult> Update(string item)
        {
            int id = int.Parse(Encrypt.Decrypt_String(item));
            if (id > 0)
            {
                OrderState update = await _context.OrderState.FindAsync(id);

                if (update != null)
                {
                    return View(update);
                }
            }

            return RedirectToAction("Index", "OrderStates", new { type = "danger", message = Alerts.Message() });
        }

        /// <summary>
        /// Post method to update an state order
        /// </summary>
        /// <param name="model">Search state order parameter</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> Update(OrderState model)
        {
            var rm = new ResponseModel();

            if (ModelState.IsValid)
            {
                try
                {
                    // Update Category object
                    _context.OrderState.Update(model);
                    await _unit.SaveChanges();
                    rm.SetResponse(true, Alerts.Message("update"));
                    rm.href = Url.Action("Index", "OrderStates", new { type = "success", message = rm.message });
                }
                catch (Exception error)
                {
                    rm.message = error.Message;
                    _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                    _logger.LogError(error, error.Message);
                }
            }
            return Json(rm);
        }

        /// <summary>
        /// Method to delete a state order
        /// </summary>
        /// <param name="item">Search state order parameter</param>
        /// <returns></returns>
        public async Task<IActionResult> SoftDelete(string item)
        {
            var rm = new ResponseModel();

            // Decrypt ID
            int id = int.Parse(Encrypt.Decrypt_String(item));

            OrderState state = await _context.OrderState.FindAsync(id);

            // Validate object
            if (state != null)
            {
                try
                {
                    state.state = !state.state;
                    _context.OrderState.Update(state);
                    await _unit.SaveChanges();
                    rm.SetResponse(true, "Estado de orden eliminado con éxito"); rm.href = Url.Action("Index", "OrderStates", new { type = "success", message = rm.message });
                }
                catch (Exception error)
                {
                    rm.message = error.Message;
                    _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                    _logger.LogError(error, error.Message);
                }
            }
            return RedirectToAction("Index", "OrderStates", new { type = "danger", message = rm.message });
        }
    }
}