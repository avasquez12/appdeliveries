﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using BACK_FOOD_MVC.Helpers.Assistants;
using BACK_FOOD_MVC.ViewModels.Products;
using FoodClasses.Helpers;
using FoodClasses.Helpers.Assistants;
using FoodClasses.Models;
using FoodClasses.Models.Entities.Deal;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SmartBreadcrumbs.Attributes;

namespace BACK_FOOD_MVC.Controllers
{
    [Authorize]
    public class ProductsController : Controller
    {
        private readonly FoodContext _context;
        private readonly IUnitOfWork _unit;
        private readonly ILogger<ProductsController> _logger;

        public ProductsController(FoodContext context, IUnitOfWork unit,
            ILogger<ProductsController> logger)
        {
            _context = context;
            _unit = unit;
            _logger = logger;
        }


        /// <summary>
        /// Product's list
        /// </summary>
        /// <param name="type">Alert type to show a message after an action</param>
        /// <param name="message">Message content</param>
        /// <returns></returns>
        [Breadcrumb("Productos")]
        public async Task<IActionResult> Index(string type = "", string message = "")
        {
            ViewBag.msg = Alerts.Alert(type, message);

            var products = await _context.Product.ToListAsync();
            return View(products);
        }

        /// <summary>
        /// Method to register a product
        /// </summary>
        /// <returns></returns>
        [Breadcrumb("Registrar", FromAction = "Index")]
        public ActionResult Register()
        {
            ViewBag.categories = new SelectList(_context.Category.Where(x => x.state), "id", "name");
            
            return View();
        }

        /// <summary>
        /// POST Method to register a new product
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> Register(AddProductVM model)
        {
            var rm = new ResponseModel();

            if (ModelState.IsValid)
            {
                try
                {
                    // Object Category
                    var product = new Product();
                    product.name = model.name;
                    product.category_id = model.category_id;
                    product.stock = model.stock;
                    product.stock_min = model.stock_min;
                    product.price = model.price;
                    product.state = true;
                    _context.Product.Add(product);

                    // Save changes
                    if (model.file != null && model.file.Length > 0)
                    {
                        try
                        {
                            string[] extensions = { ".jpg", ".png", ".jpeg" };

                            if (extensions.Contains(Path.GetExtension(model.file.FileName).ToLower()))
                            {
                                // S3 object
                                var rmfile = await S3Service.UploadFileAsync(model.file, Constants.COMPANY_NAME, Constants.FILE_PRODUCTS);

                                // Create Gallery Object
                                Gallery gallery = new Gallery();
                                gallery.name = model.file.FileName;
                                gallery.Product = product;
                                _context.Gallery.Add(gallery);

                                // Save changes
                                await _unit.SaveChanges();
                                rm.SetResponse(true, Alerts.Message("register"));
                                rm.href = Url.Action("Index", "Products", new { type = "success", message = rm.message });
                            }
                            else
                            {
                                rm.message = "El archivo no es permitido, solo de tipo .jpg, .png, .jpeg";
                            }
                        }
                        catch (Exception error)
                        {
                            rm.message = error.Message;
                        }
                    }
                    else
                    {
                        // Save changes
                        await _unit.SaveChanges();
                        rm.SetResponse(true, Alerts.Message("register"));
                        rm.href = Url.Action("Index", "Products", new { type = "success", message = rm.message });
                    }
                }
                catch (Exception error)
                {
                    rm.message = error.Message;
                    _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                    _logger.LogError(error, error.Message);
                }
            }
            return Json(rm);
        }

        /// <summary>
        /// Method to update a product
        /// </summary>
        /// <param name="item">Search Product parameter</param>
        /// <returns></returns>
        [Breadcrumb("Actualizar", FromAction = "Index")]
        public async Task<IActionResult> Update(string item)
        {
            int id = int.Parse(Encrypt.Decrypt_String(item));
            if (id > 0)
            {
                Product update = await _context.Product.FindAsync(id);
                ViewBag.categories = new SelectList(_context.Category.Where(x => x.state), "id", "name", update.category_id);

                if (update != null)
                {
                    return View(update);
                }
            }

            return RedirectToAction("Index", "Products", new { type = "danger", message = Alerts.Message() });
        }

        /// <summary>
        /// Post method to update a product
        /// </summary>
        /// <param name="model">Product object</param>
        /// <param name="file">Product's IFormFile</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> Update(Product model, IFormFile file)
        {
            var rm = new ResponseModel();

            if (ModelState.IsValid)
            {
                try
                {
                    // Update Product object
                    _context.Product.Update(model);

                    // Validate IFormFile object
                    if (file != null && file.Length > 0)
                    {
                        string[] extensions = { ".jpg", ".png", ".jpeg" };

                        if (extensions.Contains(Path.GetExtension(file.FileName).ToLower()))
                        {
                            // S3 object
                            var rmfile = await S3Service.UploadFileAsync(file, Constants.COMPANY_NAME, Constants.FILE_PRODUCTS);

                            // Update Product object
                            Gallery gallery = await _context.Gallery.Where(x => x.product_id == model.id).FirstOrDefaultAsync();
                            if (gallery != null)
                            {
                                gallery.name = file.FileName;
                                _context.Gallery.Update(gallery);
                            }
                            else
                            {
                                Gallery addGallery = new Gallery();
                                addGallery.name = file.FileName;
                                addGallery.product_id = model.id;
                                _context.Gallery.Add(addGallery);
                            }

                            // Save changes
                            await _unit.SaveChanges();
                            rm.SetResponse(true, Alerts.Message("update"));
                        }
                        else
                        {
                            rm.message = "El archivo no es permitido, solo de tipo .jpg, .png, .jpeg";
                        }
                    }
                    else
                    {
                        // Save changes
                        await _unit.SaveChanges();
                        rm.SetResponse(true, Alerts.Message("update"));
                    }
                    rm.href = Url.Action("Index", "Products", new { type = "success", message = rm.message });
                }
                catch (Exception error)
                {
                    rm.message = error.Message;
                    _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                    _logger.LogError(error, error.Message);
                }
            }
            return Json(rm);
        }

        /// <summary>
        /// Method to delete a product
        /// </summary>
        /// <param name="item">Search Product parameter</param>
        /// <returns></returns>
        public async Task<IActionResult> SoftDelete(string item)
        {
            var rm = new ResponseModel();

            // Decrypt ID
            int id = int.Parse(Encrypt.Decrypt_String(item));

            Product product = await _context.Product.FindAsync(id);

            // Validate object
            if (product != null)
            {
                try
                {
                    product.state = !product.state;
                    _context.Product.Update(product);
                    await _unit.SaveChanges();
                    rm.SetResponse(true, "Producto eliminado con éxito"); 
                    rm.href = Url.Action("Index", "Products", new { type = "success", message = rm.message });
                }
                catch (Exception error)
                {
                    rm.message = error.Message;
                    _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                    _logger.LogError(error, error.Message);
                }
            }
            return RedirectToAction("Index", "Products", new { type = "danger", message = rm.message });
        }
    }
}