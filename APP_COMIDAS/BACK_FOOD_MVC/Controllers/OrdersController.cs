﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BACK_FOOD_MVC.Helpers.Assistants;
using FoodClasses.Models;
using FoodClasses.Models.Entities.Deal;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SmartBreadcrumbs.Attributes;

namespace BACK_FOOD_MVC.Controllers
{
    [Authorize]
    public class OrdersController : Controller
    {
        private readonly FoodContext _context;
        private readonly IUnitOfWork _unit;
        private readonly ILogger<OrdersController> _logger;

        public OrdersController(FoodContext context, IUnitOfWork unit,
            ILogger<OrdersController> logger)
        {
            _context = context;
            _unit = unit;
            _logger = logger;
        }


        /// <summary>
        /// Dashboard orders
        /// </summary>
        /// <returns></returns>
        [DefaultBreadcrumb("Inicio")]
        public async Task<IActionResult> Index()
        {
            var orders = await _context.Order.Where(x => x.date != null)
                .OrderByDescending(x => x.date)
                .Take(7)
                .ToListAsync();

            // Card values
            ViewBag.orders = orders.Count();
            ViewBag.clients = _context.Client.Where(x => x.state).Count();
            ViewBag.products = _context.Product.Where(x => x.state).Count();

            // Orders per day
            var graphic = RegisteredOrders(orders);
            ViewBag.labels = graphic.labels;
            ViewBag.data = graphic.data;
            ViewBag.label = graphic.label;

            return View(orders);
        }


        /// <summary>
        /// Method for graphic per day
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private BarGraphic RegisteredOrders(List<Order> data)
        {
            var graphic = new BarGraphic(7);
            //var now = DateTime.Now;
            //var lastWeek = now.AddDays(-7);
            //var filterOrders = data.Where(x => (x.date >= lastWeek && x.date <= now));

            for (int i = 0; i < 7; i++)
            {
               graphic.data[i] = data.Where(x => x.date.Value.Day == (i + 1)).Count();
            }
            graphic.label = "Órdenes de Pedido";
            return graphic;
        }
    }
}