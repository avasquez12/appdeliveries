﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using BACK_FOOD_MVC.Helpers.Assistants;
using FoodClasses.Helpers.Assistants;
using FoodClasses.Models;
using FoodClasses.Models.Entities.Deal;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SmartBreadcrumbs.Attributes;

namespace BACK_FOOD_MVC.Controllers
{
    [Authorize]
    public class WaysToPayController : Controller
    {
        private readonly FoodContext _context;
        private readonly IUnitOfWork _unit;
        private readonly ILogger<WaysToPayController> _logger;

        public WaysToPayController(FoodContext context, IUnitOfWork unit,
            ILogger<WaysToPayController> logger)
        {
            _context = context;
            _unit = unit;
            _logger = logger;
        }


        /// <summary>
        /// Way to pay list
        /// </summary>
        /// <param name="type">Alert type to show a message after an action</param>
        /// <param name="message">Message content</param>
        /// <returns></returns>
        [Breadcrumb("Formas de pago")]
        public async Task<IActionResult> Index(string type = "", string message = "")
        {
            ViewBag.msg = Alerts.Alert(type, message);

            var ways = await _context.WayToPay.ToListAsync();
            return View(ways);
        }

        /// <summary>
        /// Method to register a WayToPay
        /// </summary>
        /// <returns></returns>
        [Breadcrumb("Registrar", FromAction = "Index")]
        public ActionResult Register()
        {
            return View();
        }

        /// <summary>
        /// POST Method to register a new WayToPay
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> Register(WayToPay model)
        {
            var rm = new ResponseModel();

            if (ModelState.IsValid)
            {
                try
                {
                    // Complete model
                    model.state = true;
                    _context.WayToPay.Add(model);

                    // Save changes
                    try
                    {
                        await _unit.SaveChanges();
                        rm.SetResponse(true, Alerts.Message("register"));
                        rm.href = Url.Action("Index", "WaysToPay", new { type = "success", message = rm.message });
                    }
                    catch (Exception error)
                    {
                        rm.message = error.Message;
                    }
                }
                catch (Exception error)
                {
                    rm.message = error.Message;
                    _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                    _logger.LogError(error, error.Message);
                }
            }
            return Json(rm);
        }

        /// <summary>
        /// Method to update a way to pay
        /// </summary>
        /// <param name="item">Search way to pay parameter</param>
        /// <returns></returns>
        [Breadcrumb("Actualizar", FromAction = "Index")]
        public async Task<IActionResult> Update(string item)
        {
            int id = int.Parse(Encrypt.Decrypt_String(item));
            if (id > 0)
            {
                WayToPay update = await _context.WayToPay.FindAsync(id);

                if (update != null)
                {
                    return View(update);
                }
            }

            return RedirectToAction("Index", "WaysToPay", new { type = "danger", message = Alerts.Message() });
        }

        /// <summary>
        /// Post method to update a way to pay order
        /// </summary>
        /// <param name="model">WayToPay Object</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> Update(WayToPay model)
        {
            var rm = new ResponseModel();

            if (ModelState.IsValid)
            {
                try
                {
                    // Update Category object
                    _context.WayToPay.Update(model);
                    await _unit.SaveChanges();
                    rm.SetResponse(true, Alerts.Message("update"));
                    rm.href = Url.Action("Index", "WaysToPay", new { type = "success", message = rm.message });
                }
                catch (Exception error)
                {
                    rm.message = error.Message;
                    _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                    _logger.LogError(error, error.Message);
                }
            }
            return Json(rm);
        }

        /// <summary>
        /// Method to delete a way to pay
        /// </summary>
        /// <param name="item">Search way to pay parameter</param>
        /// <returns></returns>
        public async Task<IActionResult> SoftDelete(string item)
        {
            var rm = new ResponseModel();

            // Decrypt ID
            int id = int.Parse(Encrypt.Decrypt_String(item));

            WayToPay way = await _context.WayToPay.FindAsync(id);

            // Validate object
            if (way != null)
            {
                try
                {
                    way.state = !way.state;
                    _context.WayToPay.Update(way);
                    await _unit.SaveChanges();
                    rm.SetResponse(true, "Forma de pago eliminada con éxito"); rm.href = Url.Action("Index", "WaysToPay", new { type = "success", message = rm.message });
                }
                catch (Exception error)
                {
                    rm.message = error.Message;
                    _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                    _logger.LogError(error, error.Message);
                }
            }
            return RedirectToAction("Index", "WaysToPay", new { type = "danger", message = rm.message });
        }
    }
}