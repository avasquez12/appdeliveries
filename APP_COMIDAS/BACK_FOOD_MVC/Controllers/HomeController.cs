﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BACK_FOOD_MVC.Models;
using Microsoft.AspNetCore.Authorization;
using BACK_FOOD_MVC.Services;
using SmartBreadcrumbs.Attributes;

namespace BACK_FOOD_MVC.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IUserService _userService;

        public HomeController(ILogger<HomeController> logger,
            IUserService userService)
        {
            _logger = logger;
            _userService = userService;
        }

        //[DefaultBreadcrumb("Inicio")]
        public IActionResult Index()
        {
            return View();
        }

        //[Breadcrumb("Users", FromAction = "Index")]
        public async Task<ActionResult> GetUsers()
        {
            var users = await _userService.GetAll();
            return View(users);
        }

        //[Breadcrumb("Privacy", FromAction = "Index")]
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


    }
}
