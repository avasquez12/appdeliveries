﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BACK_FOOD_MVC.Helpers.Assistants;
using BACK_FOOD_MVC.Services;
using BACK_FOOD_MVC.ViewModels.Users;
using FoodClasses.Helpers.Assistants;
using FoodClasses.Models;
using FoodClasses.Models.Entities.Security;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BACK_FOOD_MVC.Controllers
{
    public class AuthController : Controller
    {
        private readonly FoodContext _context;
        private readonly IUserService _userService;
        private readonly HttpContext _httpContext;
        public AuthController(FoodContext context,
            IUserService userService,
            IHttpContextAccessor httpContext)
        {
            _context = context;
            _userService = userService;
            _httpContext = httpContext.HttpContext;
        }

        [HttpGet]
        public IActionResult UserLogin()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> UserLogin(UserLoginVM model)
        {
            var rm = new ResponseModel();
            var user = _context.User.Where(x => x.email == model.email).FirstOrDefault();

            if(user != null)
            {
                if(user.email == model.email && user.password == model.password)
                {
                    var userClaims = new List<Claim>()
                    {
                        new Claim(ClaimTypes.Name, user.completeName),
                        new Claim(ClaimTypes.Email, user.email)
                    };

                    var claimsIdentity = new ClaimsIdentity(userClaims, "User IDentity");

                    // Properties
                    var authProperties = new AuthenticationProperties
                    {
                        //AllowRefresh = <bool>,
                        // Refreshing the authentication session should be allowed.

                        //ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(10),
                        // The time at which the authentication ticket expires. A 
                        // value set here overrides the ExpireTimeSpan option of 
                        // CookieAuthenticationOptions set with AddCookie.

                        //IsPersistent = true,
                        // Whether the authentication session is persisted across 
                        // multiple requests. When used with cookies, controls
                        // whether the cookie's lifetime is absolute (matching the
                        // lifetime of the authentication ticket) or session-based.

                        //IssuedUtc = <DateTimeOffset>,
                        // The time at which the authentication ticket was issued.

                        //RedirectUri = <string>
                        // The full path or absolute URI to be used as an http 
                        // redirect response value.
                    };

                    var userPrincipal = new ClaimsPrincipal(new[] { claimsIdentity });
                    await HttpContext.SignInAsync(userPrincipal);
                    //await HttpContext.SignInAsync(userPrincipal, authProperties);
                    rm.SetResponse(true, "");

                    rm.href = Url.Action("Index", "Orders");
                }
            }
            else
            {
                rm.SetResponse(false, "Usuario o contraseña incorrecta");
            }
            return Json(rm);
        }

        public async Task<IActionResult> UserLogOut()
        {
            await HttpContext.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        [Authorize]
        [HttpGet]
        public IActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> ChangePassword(UserChangePasswordVM model)
        {
            var rm = new ResponseModel();
            if(ModelState.IsValid)
            {
                var email = ((ClaimsIdentity)User.Identity).GetSpecificClaim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress");

                var user = _context.User.Where(x => x.email == email).FirstOrDefault();

                if(user != null && user.password.Equals(model.old_password))
                {
                    user.password = model.new_password;
                    rm = await _userService.Update(user);

                    if(rm.response)
                    {
                        // Send Email to confirmation proccess
                        MailVM mail = new MailVM();
                        var rmSendMail = new ResponseModel();
                        mail.destinations.Add(user.email);
                        mail.subject = string.Format("Contraseña actualizada en  {0}", Constants.COMPANY_NAME_APP);
                        mail.message = string.Format(
                            Constants.MAIL_MESSAGE_CHANGE_PASSWORD_CLIENT,
                            Constants.COMPANY_URI_APP,
                            Constants.COMPANY_NAME
                            );
                        rmSendMail = Mail.Send(mail);
                        if (!rmSendMail.response)
                        {
                            rm.message = "Ocurrió un problema al tratar de enviar el correo";
                        }
                    }
                    rm.href = Url.Action("LogOut", "Auth");
                }
            }
            return Json(rm);
        }
    }
}