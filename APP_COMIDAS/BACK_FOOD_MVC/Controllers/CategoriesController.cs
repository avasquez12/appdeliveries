﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using BACK_FOOD_MVC.Helpers.Assistants;
using BACK_FOOD_MVC.ViewModels.Categories;
using FoodClasses.Helpers;
using FoodClasses.Helpers.Assistants;
using FoodClasses.Models;
using FoodClasses.Models.Entities.Deal;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SmartBreadcrumbs.Attributes;

namespace BACK_FOOD_MVC.Controllers
{
    [Authorize]
    public class CategoriesController : Controller
    {
        private readonly FoodContext _context;
        private readonly IUnitOfWork _unit;
        private readonly ILogger<CategoriesController> _logger;

        public CategoriesController(FoodContext context, IUnitOfWork unit,
            ILogger<CategoriesController> logger)
        {
            _context = context;
            _unit = unit;
            _logger = logger;
        }

        /// <summary>
        /// Category's list
        /// </summary>
        /// <param name="type">Alert type to show a message after an action</param>
        /// <param name="message">Message content</param>
        /// <returns></returns>
        [Breadcrumb("Categorías")]
        public async Task<IActionResult> Index(string type = "", string message = "")
        {
            ViewBag.msg = Alerts.Alert(type, message);

            var categories = await _context.Category.ToListAsync();
            return View(categories);
        }

        /// <summary>
        /// Method to register a category
        /// </summary>
        /// <returns></returns>
        [Breadcrumb("Registrar", FromAction = "Index")]
        public ActionResult Register()
        {
            return View();
        }

        /// <summary>
        /// POST Method to register a new category
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> Register(AddCategoryVM model)
        {
            var rm = new ResponseModel();

            if (ModelState.IsValid)
            {
                try
                {
                    // Object Category
                    var category = new Category();
                    category.name = model.name;
                    category.name_file = model.file.FileName;
                    category.state = true;
                    _context.Category.Add(category);

                    // Save changes
                    try
                    {
                        await _unit.SaveChanges();
                        rm.SetResponse(true, Alerts.Message("register"));
                        if (rm.response)
                        {
                            if (model.file.Length > 0 && model.file != null)
                            {
                                string[] extensions = { ".jpg", ".png", ".jpeg" };

                                if (extensions.Contains(Path.GetExtension(model.file.FileName).ToLower()))
                                {
                                    // S3 object
                                    var rmfile = await S3Service.UploadFileAsync(model.file, Constants.COMPANY_NAME, Constants.FILE_CATEGORIES);
                                }
                            }
                        }

                        rm.href = Url.Action("Index", "Categories", new { type = "success", message = rm.message });
                    }
                    catch (Exception error)
                    {
                        rm.message = error.Message;
                    }
                }
                catch (Exception error)
                {
                    rm.message = error.Message;
                    _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                    _logger.LogError(error, error.Message);
                }
            }
            return Json(rm);
        }

        /// <summary>
        /// Method to update a category
        /// </summary>
        /// <param name="item">Search Category parameter</param>
        /// <returns></returns>
        [Breadcrumb("Actualizar", FromAction = "Index")]
        public async Task<IActionResult> Update(string item)
        {
            int id = int.Parse(Encrypt.Decrypt_String(item));
            if (id > 0)
            {
                Category update = await _context.Category.FindAsync(id);

                if (update != null)
                {
                    return View(update);
                }
            }

            return RedirectToAction("Index", "Categories", new { type = "danger", message = Alerts.Message() });
        }

        /// <summary>
        /// Post method to update a category
        /// </summary>
        /// <param name="model">Search Category parameter</param>
        /// <param name="file">Category's IFormFile</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> Update(Category model, IFormFile file)
        {
            var rm = new ResponseModel();

            if (ModelState.IsValid)
            {
                try
                {
                    if(file != null && file.Length > 0)
                    {
                        string[] extensions = { ".jpg", ".png", ".jpeg" };

                        if (extensions.Contains(Path.GetExtension(file.FileName).ToLower()))
                        {
                            // S3 object
                            var rmfile = await S3Service.UploadFileAsync(file, Constants.COMPANY_NAME, Constants.FILE_CATEGORIES);

                            // Update Category object
                            model.name_file = file.FileName;
                            _context.Category.Update(model);
                            await _unit.SaveChanges();
                            rm.SetResponse(true, Alerts.Message("update"));
                        }
                    }
                    else
                    {
                        // Update Category object
                        _context.Category.Update(model);
                        await _unit.SaveChanges();
                        rm.SetResponse(true, Alerts.Message("update"));
                    }

                    rm.href = Url.Action("Index", "Categories", new { type = "success", message = rm.message });
                }
                catch (Exception error)
                {
                    rm.message = error.Message;
                    _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                    _logger.LogError(error, error.Message);
                }
            }
            return Json(rm);
        }

        /// <summary>
        /// Method to delete a category
        /// </summary>
        /// <param name="item">Search Category parameter</param>
        /// <returns></returns>
        public async Task<IActionResult> SoftDelete(string item)
        {
            var rm = new ResponseModel();

            // Decrypt ID
            int id = int.Parse(Encrypt.Decrypt_String(item));

            Category category = await _context.Category.FindAsync(id);

            // Validate object
            if(category != null)
            {
                try
                {
                    category.state = !category.state;
                    _context.Category.Update(category);
                    await _unit.SaveChanges();
                    rm.SetResponse(true, "Categoría eliminada con éxito"); rm.href = Url.Action("Index", "Categories", new { type = "success", message = rm.message });
                }
                catch (Exception error)
                {
                    rm.message = error.Message;
                    _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                    _logger.LogError(error, error.Message);
                }
            }
            return RedirectToAction("Index", "Categories", new { type = "danger", message = rm.message });
        }
    }
}