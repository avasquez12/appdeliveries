﻿using FoodClasses.Helpers.Assistants;
using FoodClasses.Models;
using FoodClasses.Models.Entities.Security;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace BACK_FOOD_MVC.Services
{
    // Interface
    public interface IUserService
    {
        Task<User> Get(int id);
        Task<IEnumerable<User>> GetAll();
        Task<ResponseModel> Update(User element);
    }

    // Class
    public class UserService : IUserService
    {
        private readonly FoodContext _context;
        private readonly ILogger<UserService> _logger;
        private readonly IUnitOfWork _unit;

        public UserService(FoodContext context, ILogger<UserService> logger, IUnitOfWork unit)
        {
            _context = context;
            _logger = logger;
            _unit = unit;
        }


        public async Task<User> Get(int id)
        {
            try
            {
                var user = await _context.User.FindAsync(id);
                return user;
            }
            catch (Exception e)
            {
                _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                _logger.LogError(e, e.Message);
            }
            return null;
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            List<User> users = null;
            try
            {
                users = await _context.User.ToListAsync();
            }
            catch (Exception e)
            {
                _logger.LogInformation($"Error en la clase {this.GetType().Name} - Método {MethodBase.GetCurrentMethod().DeclaringType.Name }");
                _logger.LogError(e, e.Message);
            }
            return users;
        }

        public async Task<ResponseModel> Update(User element)
        {
            var rm = new ResponseModel();
            try
            {
                _context.User.Update(element);
                await _unit.SaveChanges();
                rm.response = true;
                rm.result = element;
                rm.message = "";
            }
            catch (Exception error)
            {
                rm.message = error.Message;
            }
            return rm;
            
        }
    }
}
